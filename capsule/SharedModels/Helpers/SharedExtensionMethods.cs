﻿using SharedModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedModel.Helpers
{
    public static class SharedExtensionMethods
    {
        public static string FileTypeName(this FileType fileType)
        {
            switch (fileType)
            {
                case FileType.HTML:
                    return "html";
                case FileType.INDEX:
                    return "index";
                case FileType.XML:
                    return "xml";
                case FileType.Image:
                    return "image";
                default:
                    throw new ArgumentException("Not valid file type: " + fileType.ToString());
            }
        }
    }
}
