﻿using SharedModel.Model;
using GalaSoft.MvvmLight;
using System;
//using Windows.UI.Xaml.Media;

namespace SharedModel.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SubSectionItemViewModel : BookViewModelBase<SubSectionItem>
    {

        public SubSectionItemViewModel(SubSectionItem model)
        {
            Model = model;
        }


        public Uri HtmlPageUri
        {
            get { return Model.HtmlPageUri; }
        }

        public string HtmlPath
        {
            get
            {
                return Model.absolutepath + @"\" + Model.HtmlPath;
            }
        }

        public String Title
        {
            get { return Model.Title; }
        }

        public String Subtitle
        {
            get { return Model.Subtitle; }
        }

        public String Description
        {
            get { return Model.Description; }
        }

        public Uri Image
        {
            get
            {
                return Model.Image;
            }
        }

        public String Colour
        {
            get
            {
                if (String.IsNullOrWhiteSpace(Model.Colour))
                    return "#FFE60000";

                return Model.Colour;
            }
        }
    }
}