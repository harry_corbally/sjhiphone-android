﻿using SharedModel.Model;
using GalaSoft.MvvmLight;
using System;
using System.Collections.ObjectModel;
using System.Linq;
//using Windows.UI.Xaml.Media;

namespace SharedModel.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ChapterItemDetailsViewModel : BookViewModelBase<ChapterItem>
    {

        public ChapterItemDetailsViewModel(ChapterItem model)
        {
            Model = model;
            Sections = new ObservableCollection<SectionItemDetailsViewModel>(model.Items.Select(i => new SectionItemDetailsViewModel(i)));
        }

        public ObservableCollection<SectionItemDetailsViewModel> Sections
        {
            get;
            private set;
        }

        public String Title
        {
            get { return Model.Title; }
        }

        public String Subtitle
        {
            get { return Model.Subtitle; }
        }

        public String Description
        {
            get { return Model.Description; }
        }

        public Uri Image
        {
            get
            {
                return Model.Image;
            }
        }

        public String Colour
        {
            get
            {
                if (String.IsNullOrWhiteSpace(Model.Colour))
                    return "#FFE60000";

                return Model.Colour;
            }
        }
    }
}