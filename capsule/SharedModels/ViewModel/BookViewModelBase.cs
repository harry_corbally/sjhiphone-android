﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using SharedModel.Model;

namespace SharedModel.ViewModel
{
    public class BookViewModelBase<T>:ViewModelBase
    {
        public T Model
        {
            get;
            set;
        }
    }
}
