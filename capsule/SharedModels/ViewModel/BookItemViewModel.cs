﻿using SharedModel.Model;
using GalaSoft.MvvmLight;
using System;
using System.Collections;
using System.Collections.Generic;
//using Windows.UI.Xaml.Media;
//using Windows.UI.Xaml.Media.Imaging;
using System.Linq;
using System.Collections.ObjectModel;

namespace SharedModel.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class BookItemViewModel : BookViewModelBase<BookItem>
    {

        public BookItemViewModel(BookItem model)
        {
            Model = model;
            Items = new ObservableCollection<ChapterItemDetailsViewModel>(model.Items.Select(i => new ChapterItemDetailsViewModel(i)));
            switch (model.Alert)
            {
                case "alert":
                    model.Image = new Uri("/assets/alert.png", UriKind.Relative);
                    model.Colour = "#FF0000";
                    break;
                case "info":
                    model.Image = new Uri("/assets/info.png", UriKind.Relative);
                    model.Colour = "#0000FF";
                    break;
                case "notification":
                    model.Image = new Uri("/assets/notify.png", UriKind.Relative);
                    model.Colour = "#00FF00";
                    break;
            }
        }


        public ObservableCollection<ChapterItemDetailsViewModel> Items
        {
            get;
            private set;
        }

        public String Title
        {
            get { return Model.Title; }
        }

        public String Subtitle
        {
            get { return Model.Subtitle; }
        }

        public String Description
        {
            get { return Model.Description; }
        }

        public Uri Image
        {
            get
            {
                return Model.Image;
            }
        }

        public String Colour
        {
            get
            {
                if (String.IsNullOrWhiteSpace(Model.Colour))
                    return "#FFE60000";

                return Model.Colour;
            }
        }
    }
}