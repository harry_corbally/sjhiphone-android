﻿using GalaSoft.MvvmLight;
using SharedModel.Model;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Command;
using Capsule.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
//using Windows.UI.Xaml.Controls;
using System.Threading.Tasks;
using System;


namespace SharedModel.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainBookGroupItemsViewModel : ViewModelBase
    {
        private readonly IBookDataService _bookDataService;
        private readonly INavigationService _navigationService;
        private readonly ISearchDataService _searchDataService;

        private Boolean _doNavigation = true;
        private Boolean _fromSearchResult = false;
        public Boolean _fromSearchResultWithSubSection = false;

        public MainBookGroupItemsViewModel(IBookDataService bookDataService, INavigationService navigationService, ISearchDataService searchDataService)
        {
            _bookDataService = bookDataService;
            _navigationService = navigationService;

            SyncCanExecute = true;

            LoadData();

            SetDisclaimerText();

            _searchDataService = searchDataService;

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SearchResult = _searchDataService.GetSearchResult(string.Empty, -1, null).Result;
                SelectedBookTitle = SelectedBookItem.Title;
            }
        }

        private void SetDisclaimerText()
        {
            DisclaimerText = String.Concat("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque dolor nisl, et pharetra massa aliquam in. Curabitur ac est porttitor, ornare ligula eget, porttitor dui. Suspendisse mollis eu nunc et aliquet. Mauris imperdiet, massa eu congue lobortis, urna justo condimentum purus, vitae viverra nisi orci in mauris. Curabitur quis varius metus. Morbi accumsan velit non tortor ultricies, quis eleifend arcu ullamcorper. Nunc eleifend eros sem, at rutrum lacus interdum in. Quisque egestas eu dolor dapibus tincidunt. Morbi mi dolor, sodales et blandit eu, mattis sit amet lacus. Morbi ac fringilla neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam facilisis porttitor vehicula. Curabitur et est urna. Vivamus eu fermentum enim.",
                                            "Aenean laoreet enim nulla, ac ullamcorper augue ultrices et. In at venenatis ligula. Etiam tristique, sem vehicula malesuada vestibulum, quam quam accumsan ligula, non venenatis est erat et magna. Sed ullamcorper cursus leo vel faucibus. Fusce blandit sapien ac velit ultrices, eget egestas tellus pharetra. Mauris bibendum magna vitae nulla cursus pulvinar. Nullam sit amet mi ac diam venenatis placerat et nec lorem. Morbi sapien turpis, ornare eu sagittis eget, pharetra sit amet orci. Nulla consectetur sit amet dolor et ullamcorper. Quisque rhoncus ultricies sem et placerat. Praesent accumsan lectus vel est laoreet pharetra. Donec ultrices sit amet justo et fringilla. Maecenas in euismod quam. Aenean eros dui, dictum eu luctus non, ultricies eget mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet aliquet diam, nec ornare enim.",
                                            "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce ac risus non purus blandit tempus sed et erat. Nunc tempor dolor sapien, sed rhoncus eros viverra eu. Praesent molestie lorem pretium varius blandit. Cras nec lacus vel eros commodo eleifend eu vitae dolor. Morbi tincidunt dolor est, ut hendrerit felis imperdiet nec. Praesent eget lorem et lectus lacinia commodo. Ut congue nisi at euismod interdum. Donec viverra dui nulla, eget condimentum lacus pharetra quis. Quisque nec lobortis sem, a convallis augue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin facilisis risus a malesuada tristique. Suspendisse sed auctor ligula, non luctus nisi. Sed accumsan bibendum leo.",
                                            "Nunc dui nunc, molestie et aliquam vel, elementum et est. Vestibulum lobortis diam tortor, nec viverra mi vulputate eu. Quisque pharetra lectus id turpis auctor, accumsan interdum lacus vestibulum. Duis nec lectus posuere, luctus diam in, viverra nibh. Pellentesque mattis tellus et diam varius mollis. Maecenas sodales vulputate neque, sed porta leo hendrerit vitae. Vestibulum vitae lorem sollicitudin quam vestibulum consectetur. Nulla ut dui erat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin bibendum ac libero in lacinia. Suspendisse viverra iaculis justo et semper.",
                                            "In nec risus in felis fermentum sodales. Curabitur ut orci a urna dignissim blandit. Quisque sit amet tincidunt leo. Sed sapien turpis, viverra ac pellentesque eget, fermentum eget neque. Vestibulum placerat hendrerit vestibulum. Aenean mattis sapien at turpis hendrerit dignissim. Donec ut fringilla ligula. Donec sit amet tincidunt enim, id iaculis erat.");
        }

        public ObservableCollection<BookGroupViewModel> BookGroups
        {
            get;
            private set;
        }

        /// <summary>
        /// The <see cref="SelectedBookGroup"/> property's name.
        /// </summary>
        public const string SelectedBookGroupPropertyName = "SelectedBookGroup";

        private BookGroupViewModel _selectedBookGroup;
        public BookGroupViewModel SelectedBookGroup
        {
            get { return _selectedBookGroup; }
            set
            {
                Set(SelectedBookGroupPropertyName, ref _selectedBookGroup, value);

                if (_selectedBookGroup != null)
                {
                    _navigationService.Navigate("BookGroupDetailsPage");
                }
            }
        }

        /// <summary>
        /// The <see cref="SelectedBookItem"/> property's name.
        /// </summary>
        public const string SelectedBookItemPropertyName = "SelectedBookItem";

        private BookItemViewModel _selectedBookItem;
        public BookItemViewModel SelectedBookItem
        {
            get { return _selectedBookItem; }
            set
            {
                Set(SelectedBookItemPropertyName, ref _selectedBookItem, value);

                SearchResult.Clear();

                if (_selectedBookItem != null)
                {
                    SelectedBookTitle = SelectedBookItem.Title;
                    _navigationService.Navigate("BookItemDetailsPage");
                }
            }
        }

        /// <summary>
        /// The <see cref="SelectedChapterItem"/> property's name.
        /// </summary>
        public const string SelectedChapterItemPropertyName = "SelectedChapterItem";

        private ChapterItemDetailsViewModel _selectedChapterItem;
        public ChapterItemDetailsViewModel SelectedChapterItem
        {
            get { return _selectedChapterItem; }
            set
            {
                Set(SelectedChapterItemPropertyName, ref _selectedChapterItem, value);

                _doNavigation = true;

                if (_selectedChapterItem != null)
                {
                    _navigationService.Navigate("ChapterItemDetailsPage");
                }
            }
        }

        /// <summary>
        /// The <see cref="SelectedSectionItem"/> property's name.
        /// </summary>
        public const string SelectedSectionItemPropertyName = "SelectedSectionItem";

        private SectionItemDetailsViewModel _selectedSectionItem;
        public SectionItemDetailsViewModel SelectedSectionItem
        {
            get { return _selectedSectionItem; }
            set
            {
                Set(SelectedSectionItemPropertyName, ref _selectedSectionItem, value);

                if (_fromSearchResultWithSubSection)
                {
                    return;
                }

                if (_fromSearchResult)
                {
                    _navigationService.Navigate("SectionWebViewPage", SearchText);
                    return;
                }

                if (_selectedSectionItem != null)
                {
                    if (_selectedSectionItem.SubSections.Any())
                    {
                        if (_selectedSectionItem.SubSections.Count() == 1)
                        {
                            SelectedSubSectionItem = _selectedSectionItem.SubSections.First();
                        }
                        else
                        {
                            _navigationService.Navigate("SectionItemDetailsPage");
                        }
                    }
                    else
                    {
                        if (SelectedChapterItem.Sections.Any())
                            _selectedSectionItemIndex = SelectedChapterItem.Sections.IndexOf(SelectedSectionItem);
                        else
                            _selectedSectionItemIndex = 0;

                        if (_doNavigation)
                            _navigationService.Navigate("SectionWebViewPage", SearchText);

                        _doNavigation = false;
                    }
                }
            }
        }

        private Int32 _selectedSectionItemIndex;
        public Int32 SelectedSectionItemIndex
        {
            get
            {
                return _selectedSectionItemIndex;
            }
        }


        /// <summary>
        /// The <see cref="SelectedSubSectionItem"/> property's name.
        /// </summary>
        public const string SelectedSubSectionItemPropertyName = "SelectedSubSectionItem";

        private SubSectionItemViewModel _selectedSubSectionItem;
        public SubSectionItemViewModel SelectedSubSectionItem
        {
            get { return _selectedSubSectionItem; }
            set
            {
                Set(SelectedSubSectionItemPropertyName, ref _selectedSubSectionItem, value);

                if (!_fromSearchResultWithSubSection)
                {
                    if (SelectedSectionItem.SubSections.Any())
                        _selectedSubSectionItemIndex = SelectedSectionItem.SubSections.IndexOf(SelectedSubSectionItem);
                    else
                        _selectedSubSectionItemIndex = 0;

                    SearchText = String.Empty;
                }

                if (_selectedSubSectionItem != null)
                    _navigationService.Navigate("SubSectionWebViewPage", SearchText);
            }
        }

        private Int32 _selectedSubSectionItemIndex = 0;
        public Int32 SelectedSubSectionItemIndex
        {
            get
            {
                return _selectedSubSectionItemIndex;
            }
        }

        /// <summary>
        /// The <see cref="ErrorMsg"/> property's name.
        /// </summary>
        public const string ErrorMsgPropertyName = "ErrorMsg";

        private String _errorMsg;
        public String ErrorMsg
        {
            get { return _errorMsg; }
            set
            {
                Set(ErrorMsgPropertyName, ref _errorMsg, value);
            }
        }

        public async Task LoadData()
        {
            var groups = await _bookDataService.GetBookGroups();

            if (groups != null)
            {
                BookGroups=new ObservableCollection<BookGroupViewModel>(groups.Select(g => new BookGroupViewModel(g, _navigationService)));
                //observable collection and associated sub collections now 
                //created potentially after UI loaded (notice await)
                //therefore notify the bindings that the bookgroups is no longer null
                RaisePropertyChanged<ObservableCollection<BookGroupViewModel>>(() => this.BookGroups);
 
                if (IsInDesignMode)
                {
                    if (BookGroups.Any())
                    {
                        SelectedBookGroup = BookGroups[0];

                        if (SelectedBookGroup.Items.Any())
                        {
                            SelectedBookItem = SelectedBookGroup.Items.First();

                            if (SelectedBookItem.Items.Any())
                            {
                                SelectedChapterItem = SelectedBookItem.Items.First();

                                if (SelectedChapterItem.Sections.Any())
                                {
                                    SelectedSectionItem = SelectedChapterItem.Sections.First();

                                    if (SelectedSectionItem.SubSections.Any())
                                    {
                                        SelectedSubSectionItem = SelectedSectionItem.SubSections.First();
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                ErrorMsg = "Error occured on loading resources!";
            }
        }

        private RelayCommand<BookGroupViewModel> _changeSelectedBookGroupCommand;
        public RelayCommand<BookGroupViewModel> ChangeSelectedBookGroupCommand
        {
            get
            {
                return _changeSelectedBookGroupCommand
                    ?? (_changeSelectedBookGroupCommand = new RelayCommand<BookGroupViewModel>(
                            group =>
                            {
                                SelectedBookGroup = group;
                            }));
            }
        }


        private RelayCommand<BookItemViewModel> _navigateToBookCommand;
        public RelayCommand<BookItemViewModel> NavigateToBookCommand
        {
            get
            {
                return _navigateToBookCommand
                    ?? (_navigateToBookCommand = new RelayCommand<BookItemViewModel>(
                            book =>
                            {
                                _doNavigation = true;
                                SelectedBookItem = book;
                            }));
            }
        }

        private RelayCommand<ChapterItemDetailsViewModel> _navigateToChapterCommand;
        public RelayCommand<ChapterItemDetailsViewModel> NavigateToChapterCommand
        {
            get
            {
                return _navigateToChapterCommand
                    ?? (_navigateToChapterCommand = new RelayCommand<ChapterItemDetailsViewModel>(
                            chapter =>
                            {
                                //currIndex = 0;
                                _doNavigation = true;
                                SelectedChapterItem = chapter;
                            }));
            }
        }

        private RelayCommand<SectionItemDetailsViewModel> _navigateToSectionCommand;
        public RelayCommand<SectionItemDetailsViewModel> NavigateToSectionCommand
        {
            get
            {
                return _navigateToSectionCommand
                    ?? (_navigateToSectionCommand = new RelayCommand<SectionItemDetailsViewModel>(
                            section =>
                            {
                                _doNavigation = true;
                                SelectedSectionItem = section;
                            }));
            }
        }

        private RelayCommand<SubSectionItemViewModel> _navigateToSubSectionCommand;
        public RelayCommand<SubSectionItemViewModel> NavigateToSubSectionCommand
        {
            get
            {
                return _navigateToSubSectionCommand
                    ?? (_navigateToSubSectionCommand = new RelayCommand<SubSectionItemViewModel>(
                            subSection =>
                            {
                                SelectedSubSectionItem = subSection;
                            }));
            }
        }

        private RelayCommand _subSectionChangedCommand;
        public RelayCommand SubSectionChangedCommand
        {
            get
            {
                return _subSectionChangedCommand
                    ?? (_subSectionChangedCommand = new RelayCommand(
                            () =>
                            {
                                SubSectionItemViewModel x = null;
                            }));
            }
        }

        private RelayCommand _refreshCommand;
        public RelayCommand RefreshCommand
        {
            get
            {
               return _refreshCommand = new RelayCommand(
                    () =>
                    {
                        LoadData();
                    }
                    );
            }
        }

        private RelayCommand _navigateToBookGroupsCommand;
        public RelayCommand NavigateToBookGroupsCommand
        {
            get
            {
                return _navigateToBookGroupsCommand = new RelayCommand(
                     () =>
                     {
                         _navigationService.Navigate("MainBookGroupItemsView");
                     }
                     );
            }
        }


        #region "Search Functionality"

        public string SelectedBookTitle { get; set; }

        private List<Occurence> _searchResult;
        public List<Occurence> SearchResult
        {
            get
            {
                return _searchResult ?? (_searchResult = new List<Occurence>());
            }
            set
            {
                Set("SearchResult", ref _searchResult, value);
                HasResult = _searchResult.Count > 0;
            }
        }

        private bool _hasResult;
        public bool HasResult
        {
            get { return _hasResult; }
            set { Set("HasResult", ref _hasResult, value); }
        }

        private String _searchTextTemp { get; set; }
        public String SearchText { get; set; }

        private RelayCommand<string> _searchCommand;
        public RelayCommand<string> SearchCommand
        {
            get
            {
                return _searchCommand
                    ?? (_searchCommand = new RelayCommand<string>(
                        async (searchText) =>
                        {
                            SearchText = searchText;
                            _searchTextTemp = searchText;

                            if (!string.IsNullOrWhiteSpace(searchText) && searchText.Length >= 1)
                            {
                                SearchResult = new List<Occurence>();

                                SearchResult = await _searchDataService.GetSearchResult(searchText, SelectedBookItem.Model.Id, BookGroups);
                            }
                        }
                        ));
            }
        }

        private RelayCommand<Occurence> _navigateToOccuranceCommand;
        public RelayCommand<Occurence> NavigateToOccuranceCommand
        {
            get
            {
                return _navigateToOccuranceCommand
                    ?? (_navigateToOccuranceCommand = new RelayCommand<Occurence>(
                        (occurance) =>
                        {
                            //var mainVM = ((SharedModel.ViewModel.ViewModelLocator)(Resources["Locator"])).Main;

                            //mainVM.SelectedChapterItem = mainVM.BookGroups[1].Items[1].Items[0];

                            var x = SelectedBookItem;
                            var chapter = SelectedBookItem.Items.SingleOrDefault(p => p.Model.Id == occurance.ChapterId);
                            var section = chapter.Sections.SingleOrDefault(p => p.Model.Id == occurance.SectionId);

                            if (occurance.SubSectionId > 0)
                            {
                                _fromSearchResultWithSubSection = true;
                                var subSection = section.SubSections.SingleOrDefault(p => p.Model.Id == occurance.SubSectionId);

                                SearchText = _searchTextTemp;

                                SelectedSectionItem = section;
                                SelectedSubSectionItem = subSection;
                            }
                            else
                            {
                                _fromSearchResult = true;
                                _fromSearchResultWithSubSection = false;
                                SelectedSectionItem = section;
                            }
                        }
                ));
            }
        }

        #endregion

        #region "About Page"

        private Boolean _isSyncError;
        public Boolean IsSyncError
        {
            get { return _isSyncError; }
            set { Set("IsSyncError", ref _isSyncError, value); }
        }

        private Boolean _syncCanExecute = true;
        public Boolean SyncCanExecute
        {
            get { return _syncCanExecute; }
            set
            {
                Set("SyncCanExecute", ref _syncCanExecute, value);
                SyncInProgress = !_syncCanExecute;
            }
        }

        private Boolean _syncInProgress = false;
        public Boolean SyncInProgress
        {
            get { return _syncInProgress; }
            set { Set("SyncInProgress", ref _syncInProgress, value); }
        }

        private String _syncProgressStep;
        public String SyncProgressStep
        {
            get { return _syncProgressStep; }
            set { Set("SyncProgressStep", ref _syncProgressStep, value); }
        }

        public async Task<SyncResult> DoSynchronization(DateTime? lastSyncDate)
        {
            SyncResult syncResult = await _bookDataService.StartSynchronization(lastSyncDate, this, !lastSyncDate.HasValue);
            SyncCanExecute = true;

            return syncResult;
        }

        public async Task<DateTime?> GetServerDate()
        {
            DateTime? serverDate = await _bookDataService.GetServerDate();
            SyncCanExecute = true;
            return serverDate;
        }

        #endregion

        #region "Disclaimer Page"

        public String DisclaimerText { get; set; }

        #endregion
    }

}