﻿using SharedModel.Model;
using Capsule.Services.Interfaces;
//using Capsule.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
//using Capsule.ViewModel;
//using Windows.UI.Xaml.Media;
//using Windows.UI.Xaml.Media.Imaging;

namespace SharedModel.ViewModel
{
    public class BookGroupViewModel : BookViewModelBase<BookGroup>
    {
        private INavigationService _navigationService;

        public BookGroupViewModel(BookGroup model, INavigationService navigationService)
        {
            Model = model;
            Items = new ObservableCollection<BookItemViewModel>(model.Items.Select(i => new BookItemViewModel(i)));
            _navigationService = navigationService;

        }

        /// <summary>
        /// The <see cref="SelectedBookItem"/> property's name.
        /// </summary>
        public const string SelectedBookItemPropertyName = "SelectedBookItem";

        private BookItemViewModel _selectedBookItem;
        public BookItemViewModel SelectedBookItem
        {
            get { return _selectedBookItem; }
            set
            {
                Set(SelectedBookItemPropertyName, ref _selectedBookItem, value);

                if (_selectedBookItem != null)
                {
                    _navigationService.Navigate("BookItemDetailsPage");
                }
            }
        }


        public IEnumerable<BookItemViewModel> TopItems
        {
            get { return Items.Take(5); }
        }

        public ObservableCollection<BookItemViewModel> Items
        {
            get;
            private set;
        }

        public String Title
        {
            get { return Model.Title; }
        }

        public String Subtitle
        {
            get { return Model.Subtitle; }
        }

        public String Description
        {
            get { return Model.Description; }
        }

        public Uri Image
        {
            get
            {
                return Model.Image;
            }
        }
        
    }
}
