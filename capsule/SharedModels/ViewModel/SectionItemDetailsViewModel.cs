﻿using SharedModel.Model;
using GalaSoft.MvvmLight;
using System;
using System.Collections.ObjectModel;
using System.Linq;
//using Windows.UI.Xaml.Media;

namespace SharedModel.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SectionItemDetailsViewModel : BookViewModelBase<SectionItem>
    {
        public SectionItemDetailsViewModel(SectionItem model)
        {
            Model = model;
            SubSections = new ObservableCollection<SubSectionItemViewModel>(model.Items.Select(i => new SubSectionItemViewModel(i)));
        }

       
        public Uri HtmlPageUri
        {
            get
            {
                return Model.HtmlPageUri;
            }
        }

        public string HtmlPath
        {
            get
            {
                return Model.absolutepath + @"\" + Model.HtmlPath;
            }
        }

        public String SubSectionsCountStr
        {
            get { return "Nr. of sub-sections: " + SubSections.Count; }
        }

        public ObservableCollection<SubSectionItemViewModel> SubSections
        {
            get;
            private set;
        }

        public String Title
        {
            get { return Model.Title; }
        }

        public String Subtitle
        {
            get { return Model.Subtitle; }
        }

        public String Description
        {
            get { return Model.Description; }
        }

        public Uri Image
        {
            get
            {
                return Model.Image;
            }
        }

        public String Colour
        {
            get
            {
                if (String.IsNullOrWhiteSpace(Model.Colour))
                    return "#FFE60000";

                return Model.Colour;
            }
        }
    }
}