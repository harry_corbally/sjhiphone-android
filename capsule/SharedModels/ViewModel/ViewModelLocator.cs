﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="using:ProjectForTemplates.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using SharedModel.Model;
using Capsule.Services;
using Capsule.Services.Design;
using Capsule.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace SharedModel.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {

        public static Func<INavigationService> navService = ()=>null;
        public static Func<IBookDataService> bookService = () => null;
        public static Func<ISearchDataService> searchService = () => null;

        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IBookDataService, DesignBookDataService>();
                SimpleIoc.Default.Register<INavigationService, DesignNavigationService>();
                SimpleIoc.Default.Register<ISearchDataService, DesignSearchDataService>();
            }
            else
            {
                //SimpleIoc.Default.Register<IBookDataService, DesignBookDataService>();
                //SimpleIoc.Default.Register<INavigationService, DesignNavigationService>();
                SimpleIoc.Default.Register<INavigationService>(navService);
                SimpleIoc.Default.Register<IBookDataService>(bookService);
                SimpleIoc.Default.Register<ISearchDataService>(searchService);
                //SimpleIoc.Default.Register<IBookDataService, BookDataService>();
                //SimpleIoc.Default.Register<IBookDataService, DesignBookDataService>();

                //SimpleIoc.Default.Register<INavigationService, NavigationService>();
            }

            SimpleIoc.Default.Register<MainBookGroupItemsViewModel>();
        }

        public static void ReRegisterServices()
        {
            SimpleIoc.Default.Unregister<INavigationService>();
            SimpleIoc.Default.Register<INavigationService>(navService);
            SimpleIoc.Default.Unregister<IBookDataService>();
            //SimpleIoc.Default.Register<IBookDataService, DesignBookDataService>();
            SimpleIoc.Default.Register<IBookDataService>(bookService);
            SimpleIoc.Default.Unregister<ISearchDataService>();
            SimpleIoc.Default.Register<ISearchDataService>(searchService);
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainBookGroupItemsViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainBookGroupItemsViewModel>();
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
        }
    }
}