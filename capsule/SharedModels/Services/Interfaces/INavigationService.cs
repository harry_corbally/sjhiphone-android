﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capsule.Services.Interfaces
{
    public interface INavigationService
    {
        void Navigate(string nav);
        void Navigate(string nav, object parameter);
        void GoBack();
    }
}
