﻿using SharedModel.Model;
using SharedModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capsule.Services.Interfaces
{
    public interface ISearchDataService
    {
        Task<List<Occurence>> GetSearchResult(String searchText, Int32 bookId, IEnumerable<BookGroupViewModel> bookGroups);
    }
}
