﻿using SharedModel.Model;
using SharedModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capsule.Services.Interfaces
{
    public interface IBookDataService
    {
        Task<IList<BookGroup>> GetBookGroups();
        Task<SyncResult> StartSynchronization(DateTime? lastSyncDate, MainBookGroupItemsViewModel mainVM, Boolean isFullSync);
        Task<DateTime?> GetServerDate();
    }
}
