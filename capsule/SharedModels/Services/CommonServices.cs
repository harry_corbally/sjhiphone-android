﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SharedModel.Model;
using System.IO;
using SharedModel.ViewModel;

namespace SharedModel.Services
{
    public static class extns
    {
        public static R CheckNull<T,R>(this T item,Func<T,R> func, R dfault)
        {
            if (item == null)
                return dfault;
            return func(item);
        }
    }


    public class CommonServices
    {

        public static IEnumerable<BookGroup> ParseXml(String xmlContent, string absolutePath)
        {
            XDocument xDoc = XDocument.Parse(xmlContent);
            var _bookGroups = new List<BookGroup>();
            foreach (var group in xDoc.Descendants("Groups").Descendants("Group"))
            {

                BookGroup bookGroup = new BookGroup();
                bookGroup.IsAlertNotificationGroup = group.Element("IsAlert").CheckNull(x => Boolean.Parse(x.Value), false);
                SetCommonDataProperties(bookGroup, group,"", absolutePath, "#FFE60000");
                GetBooksFromXml(bookGroup,group,absolutePath);

                _bookGroups.Add(bookGroup);
            }

            return _bookGroups;
        }

        private static void SetCommonDataProperties(ICommonData dataitem, XElement elem, string parentFolder, string absolutePath, string bookColor)
        {
            dataitem.Id = elem.Element("Id").CheckNull(x=>Int32.Parse(x.Value),-1);
            dataitem.Title = elem.Element("Title").CheckNull(x => x.Value, "");
            dataitem.Subtitle = elem.Element("Subtitle").CheckNull(x => x.Value, "");
			dataitem.Folder = String.IsNullOrEmpty(parentFolder) ? dataitem.Id.ToString() : parentFolder + @"\" + dataitem.Id;
            dataitem.Image = new Uri(absolutePath + @"\" + dataitem.Folder + @"\" + elem.Element("ImageUrl").CheckNull(x=>x.Value,""));
            dataitem.Description = elem.Element("Description").CheckNull(x => x.Value, "");
            dataitem.Alert = elem.Attribute("level").CheckNull(x => x.Value, "");

            XElement colorElement = elem.Element("Colour");

            String color = String.Empty;

            if (colorElement != null)
                color = colorElement.Value;

            dataitem.Colour = String.IsNullOrWhiteSpace(color) ? bookColor : color;
        }


        private static BookGroup GetBooksFromXml(BookGroup bookGroup, XElement group, string absolutePath)
        {
            ObservableCollection<BookItem> books = new ObservableCollection<BookItem>();

            foreach (var book in group.Descendants("Books").Descendants("Book"))
            {
                var folderName = book.Element("Id").Value;
                string bookFolder = bookGroup.Folder + @"\" + folderName;

                BookItem bookItem = new BookItem();
                bookItem.ParentItem = bookGroup;
                SetCommonDataProperties(bookItem, book, bookGroup.Folder, absolutePath, "#FFE60000");
                GetChaptersFromXml(bookItem,book, bookFolder, absolutePath);

                books.Add(bookItem);
            }

            bookGroup.Items = books;

            return bookGroup;
        }

        private static void GetChaptersFromXml(BookItem book, XElement bookNode, string bookFolder, string absolutePath)
        {
            ObservableCollection<ChapterItem> chapters = new ObservableCollection<ChapterItem>();

            foreach (var chapter in bookNode.Descendants("Chapters").Descendants("Chapter"))
            {
                String folderName = chapter.Element("Id").Value;

                string chapterFolder = bookFolder + @"\" + folderName;

                ChapterItem chapterItem = GetSectionsFromXml(book, chapter, chapterFolder, absolutePath);
                SetCommonDataProperties(chapterItem, chapter, bookFolder, absolutePath, book.Colour);
                
                chapterItem.Parent = book;
                chapters.Add(chapterItem);
            }

            book.Items = chapters;
        }

        private static ChapterItem GetSectionsFromXml(BookItem book, XElement chapterNode, string chapterFolder, string absolutePath)
        {
            ChapterItem chapter = new ChapterItem();
            ObservableCollection<SectionItem> sections = new ObservableCollection<SectionItem>();

            foreach (var section in chapterNode.Descendants("Sections").Descendants("Section"))
            {
                String folderName = section.Element("Id").Value;

                string sectionFolder = chapterFolder + @"\" + folderName;

                SectionItem sectionItem = GetSubSectionsFromXml(section, sectionFolder, absolutePath);
                SetCommonDataProperties(sectionItem, section, chapterFolder, absolutePath, book.Colour);

                sectionItem.HtmlPath = section.Element("HtmlPath").Value;
                sectionItem.SetHtmlPageUri(sectionFolder);
                sectionItem.absolutepath = @"CapsuleMainFolder\Content" + @"\" + sectionFolder;
                
                sectionItem.Parent = chapter;

                sections.Add(sectionItem);
            }
            chapter.Items = sections;

            return chapter;
        }

        private static SectionItem GetSubSectionsFromXml(XElement sectionNode, string sectionFolder, string absolutePath)
        {
            SectionItem section = new SectionItem();

            ObservableCollection<SubSectionItem> subsections = new ObservableCollection<SubSectionItem>();

            foreach (var subsection in sectionNode.Descendants("SubSections").Descendants("SubSection"))
            {
                Int32 folderName = Int32.Parse(subsection.Element("Id").Value);

                string subSectionFolder = sectionFolder + @"\" + folderName;

                SubSectionItem subSectionItem = new SubSectionItem();

                subSectionItem.Id = folderName;
                subSectionItem.Title = subsection.Element("Title").Value;
                subSectionItem.Subtitle = subsection.Element("Subtitle").Value;
                subSectionItem.Image = new Uri(absolutePath + @"\" + subSectionFolder + @"\" + subsection.Element("ImageUrl").Value);

                subSectionItem.HtmlPath = subsection.Element("HtmlPath").Value;
                subSectionItem.SetHtmlPageUri(subSectionFolder);
                subSectionItem.absolutepath = @"CapsuleMainFolder\Content" + @"\" + subSectionFolder;

                subSectionItem.Parent = section;

                subsections.Add(subSectionItem);
            }
            section.Items = subsections;

            return section;
        }

        public static async Task<IEnumerable<SearchResultItem>> ParseIndexFile(String indexFileContent, IEnumerable<BookGroupViewModel> bookGroups)
        {
            var result = new List<SearchResultItem>();

            using (StringReader sr = new StringReader(indexFileContent))
            {
                String line = await sr.ReadLineAsync();

                while (line != null)
                {
                    String[] s = line.Split(':');

                    if (s.Length != 2)
                        throw new Exception("Index file line is not valid! (" + line + ")");

                    var item = new SearchResultItem();

                    String word = s[0];
                    String occurances = s[1];

                    item.Word = word;
                    item.Occurences = GetOccurances(occurances, bookGroups);

                    result.Add(item);

                    line = await sr.ReadLineAsync();
                }
            }

            return result;
        }

        private static List<Occurence> GetOccurances(String occurances, IEnumerable<BookGroupViewModel> bookGroups)
        {
            var result = new List<Occurence>();

            String[] items = occurances.Split(',');

            foreach (var item in items)
            {
                String trimmedItem = item.Trim('{', '}');

                String[] splits = trimmedItem.Split('>');

                if (splits.Length != 6)
                    throw new Exception("Not valid occurance (" + item + ")");

                var occurance = new Occurence();

                // group
                occurance.GroupId = Int32.Parse(splits[0]);
                BookGroupViewModel bookGroupVM = bookGroups.SingleOrDefault(p => p.Model.Id == occurance.GroupId);
                
                if (bookGroupVM != null)
                    occurance.GroupTitle = bookGroupVM.Model.Title;

                // book
                occurance.BookId = Int32.Parse(splits[1]);
                BookItem bookItem = bookGroupVM.Model.Items.SingleOrDefault(p => p.Id == occurance.BookId);

                if (bookItem != null)
                    occurance.BookTitle = bookItem.Title;

                // chapter
                occurance.ChapterId = Int32.Parse(splits[2]);
                ChapterItem chItem = bookItem.Items.SingleOrDefault(p => p.Id == occurance.ChapterId);

                if (chItem != null)
                    occurance.ChapterTitle = chItem.Title;

                // section
                occurance.SectionId = Int32.Parse(splits[3]);
                SectionItem sectionItem = chItem.Items.SingleOrDefault(p => p.Id == occurance.SectionId);

                if (sectionItem != null)
                {
                    occurance.SectionTitle = sectionItem.Title;

                    // subsection
                    occurance.SubSectionId = Int32.Parse(splits[4]);
                    SubSectionItem subSectionItem = sectionItem.Items.SingleOrDefault(p => p.Id == occurance.SubSectionId);

                    if (subSectionItem != null)
                        occurance.SubSectionTitle = subSectionItem.Title;
                }

                

                // html

                result.Add(occurance);
            }

            return result;
        }

		// non viewmodel based search methods

		public static async Task<IEnumerable<SearchResultItem>> ParseIndexFile(String indexFileContent, IEnumerable<BookGroup> bookGroups)
		{
			var result = new List<SearchResultItem>();

			using (StringReader sr = new StringReader(indexFileContent))
			{
				String line = await sr.ReadLineAsync();

				while (line != null)
				{
					String[] s = line.Split(':');

					if (s.Length != 2)
						throw new Exception("Index file line is not valid! (" + line + ")");

					var item = new SearchResultItem();

					String word = s[0];
					String occurances = s[1];

					item.Word = word;
					item.Occurences = GetOccurances(occurances, bookGroups);

					result.Add(item);

					line = await sr.ReadLineAsync();
				}
			}

			return result;
		}

		private static List<Occurence> GetOccurances(String occurances, IEnumerable<BookGroup> bookGroups)
		{
			var result = new List<Occurence>();

			String[] items = occurances.Split(',');

			foreach (var item in items)
			{
				String trimmedItem = item.Trim('{', '}');

				String[] splits = trimmedItem.Split('>');

				if (splits.Length != 6)
					throw new Exception("Not valid occurance (" + item + ")");

				var occurance = new Occurence();

				// group
				occurance.GroupId = Int32.Parse(splits[0]);
				BookGroup bookGroupVM = bookGroups.SingleOrDefault(p => p.Id == occurance.GroupId);

				if (bookGroupVM != null)
					occurance.GroupTitle = bookGroupVM.Title;

				// book
				occurance.BookId = Int32.Parse(splits[1]);
				BookItem bookItem = bookGroupVM.Items.SingleOrDefault(p => p.Id == occurance.BookId);

				if (bookItem != null)
					occurance.BookTitle = bookItem.Title;

				// chapter
				occurance.ChapterId = Int32.Parse(splits[2]);
				ChapterItem chItem = bookItem.Items.SingleOrDefault(p => p.Id == occurance.ChapterId);

				if (chItem != null)
					occurance.ChapterTitle = chItem.Title;

				// section
				occurance.SectionId = Int32.Parse(splits[3]);
				SectionItem sectionItem = chItem.Items.SingleOrDefault(p => p.Id == occurance.SectionId);

				if (sectionItem != null)
				{
					occurance.SectionTitle = sectionItem.Title;

					// subsection
					occurance.SubSectionId = Int32.Parse(splits[4]);
					SubSectionItem subSectionItem = sectionItem.Items.SingleOrDefault(p => p.Id == occurance.SubSectionId);

					if (subSectionItem != null)
						occurance.SubSectionTitle = subSectionItem.Title;
				}

				// html

				result.Add(occurance);
			}

			return result;
		}
    }
}
