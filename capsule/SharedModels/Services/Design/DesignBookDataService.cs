﻿using SharedModel.Model;
using Capsule.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedModel.ViewModel;

namespace Capsule.Services.Design
{
    public class DesignBookDataService : IBookDataService
    {
        public Task<IList<BookGroup>> GetBookGroups()
        {
            var groups = new List<BookGroup>();

            for (int i = 0; i < 5; i++)
            {
                BookGroup group = new BookGroup();

                group.Title = "Book Group more text " + i;
                
                //group.ImageUrl = "Assets/LightGray.png";

                //if (i % 3 == 1)
                //    group.ImageUrl = "Assets/LightGray.png";
                //else if (i % 3 == 2)
                //    group.ImageUrl = "Assets/MediumGray.png";
                //else if (i % 3 == 0)
                //    group.ImageUrl = "Assets/DarkGray.png";

                for (int j = 0; j < 7; j++)
                {
                    BookItem book = new BookItem();

                    book.Title = "Book more text" + j + "(Grp" + i + ")";
                    book.Subtitle = "Book ST more text " + j + "(Grp" + i + ")";
                    book.Colour = "#1ED9FF";

                    //book.ImageUrl = "Assets/Images/Group1/Book1/cover.png";

                    /*
                    if (j % 3 == 1)
                        book.ImageUrl = "Assets/MediumGray.png";
                    else if (j % 3 == 2)
                        book.ImageUrl = "Assets/DarkGray.png";
                    else if (j % 3 == 0)
                        book.ImageUrl = "Assets/LightGray.png";
                    */
                    for (int k = 0; k < 10; k++)
                    {
                        ChapterItem chapter = new ChapterItem();

                        chapter.Title = "Chapter more text " + k + "(Grp" + i + "_Book" + j + ")";
                        chapter.Subtitle = "Chapter ST more text " + k + "(Grp" + i + "_Book" + j + ")";
                        chapter.Colour = book.Colour;
                        //if (k % 3 == 1)
                        //    chapter.ImageUrl = "Assets/Images/Group1/Book1/Chapters/ch1_300_225.png";
                        //else if (k % 3 == 2)
                        //    chapter.ImageUrl = "Assets/Images/Group1/Book1/Chapters/ch2_300_225.png";
                        //else if (k % 3 == 0)
                        //    chapter.ImageUrl = "Assets/Images/Group1/Book1/Chapters/ch3_300_225.png";

                        for (int m = 0; m < 7; m++)
                        {
                            SectionItem section = new SectionItem();

                            section.Title = "Section more text " + m + "(Grp" + i + "_Book" + j + "_Ch" + k + ")";
                            section.Subtitle = "Section ST more text " + m + "(Grp" + i + "_Book" + j + "_Ch" + k + ")";
                            section.Colour = chapter.Colour;

                            if (m % 3 == 1)
                            {
                                //section.ImageUrl = "Assets/Images/Group1/Book1/Chapters/ch1_300_225.png";
                                //section.HtmlPath = "Assets/HtmlPages/Design/Section01.html";
                            }
                            else if (m % 3 == 2)
                            {
                                //section.ImageUrl = "Assets/Images/Group1/Book1/Chapters/ch2_300_225.png";
                                //section.HtmlPath = "Assets/HtmlPages/Design/Section02.html";
                            }
                            else if (m % 3 == 0)
                            {
                                //section.ImageUrl = "Assets/Images/Group1/Book1/Chapters/ch3_300_225.png";

                                for (int n = 0; n < 3; n++)
                                {
                                    SubSectionItem subSection = new SubSectionItem();

                                    subSection.Title = "Sub-Section more text " + n + "(Grp" + i + "_Book" + j + "_Ch" + k + "_Section" + m + ")";
                                    subSection.Colour = section.Colour;
                                    //subSection.ImageUrl = "Assets/subsection-icon.png";

                                    //if (n % 3 == 1)
                                    //    subSection.HtmlPath = "Assets/HtmlPages/Design/SubSection01.html";
                                    //else if (n % 3 == 2)
                                    //    subSection.HtmlPath = "Assets/HtmlPages/Design/SubSection02.html";
                                    //else if (n % 3 == 0)
                                    //    subSection.HtmlPath = "Assets/HtmlPages/Design/SubSection03.html";

                                    section.Items.Add(subSection);
                                }
                            }

                            chapter.Items.Add(section);
                        }

                        book.Items.Add(chapter);
                    }

                    group.Items.Add(book);
                }

                groups.Add(group);

            }

            return Task.FromResult((IList<BookGroup>)groups);
        }


        public Task<SyncResult> StartSynchronization(DateTime? lastSyncDate, MainBookGroupItemsViewModel mainVM, Boolean isFullSync)
        {
            throw new NotImplementedException();
        }


        public Task<DateTime?> GetServerDate()
        {
            throw new NotImplementedException();
        }
    }
}
