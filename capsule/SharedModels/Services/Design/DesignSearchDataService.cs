﻿using Capsule.Services.Interfaces;
using SharedModel.Model;
using SharedModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capsule.Services.Design
{
    public class DesignSearchDataService : ISearchDataService
    {
        public Task<List<Occurence>> GetSearchResult(String searchText, Int32 bookId, IEnumerable<BookGroupViewModel> bookGroups)
        {
            SearchResultItem item = new SearchResultItem();

            item.Word = "heart";

            Occurence occure = new Occurence();
            occure.GroupId = 1;
            occure.GroupTitle = "Prescriber's Guides";
            occure.BookId = 1;
            occure.BookTitle = "Prescriber's Guide 2011";
            occure.ChapterId = 1;
            occure.ChapterTitle = "Gastrointestinal";
            occure.SectionId = 1;
            occure.SectionTitle = "Prescribing advice for Proton Pump Inhibitors";

            item.Occurences.Add(occure);

            Occurence occure2 = new Occurence();
            occure2.GroupId = 1;
            occure2.GroupTitle = "Prescriber's Guides";
            occure2.BookId = 1;
            occure2.BookTitle = "Prescriber's Guide 2011";
            occure2.ChapterId = 1;
            occure2.ChapterTitle = "Gastrointestinal";
            occure2.SectionId = 2;
            occure2.SectionTitle = "Dyspepsia";

            item.Occurences.Add(occure2);

            Occurence occure3 = new Occurence();
            occure3.GroupId = 1;
            occure3.GroupTitle = "Prescriber's Guides";
            occure3.BookId = 1;
            occure3.BookTitle = "Prescriber's Guide 2011";
            occure3.ChapterId = 2;
            occure3.ChapterTitle = "Cardiovascular";
            occure3.SectionId = 4;
            occure3.SectionTitle = "Treatment Guidelines";
            occure3.SubSectionId = 1;
            occure3.SubSectionTitle = "Chronic Heart Failure";

            item.Occurences.Add(occure3);

            return Task.FromResult<List<Occurence>>(item.Occurences);
        }
    }
}
