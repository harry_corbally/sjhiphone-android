﻿using Newtonsoft.Json;
using SharedModel.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Formatting;

namespace SharedModel.Services
{
    public class SyncServices
    {
        public static async Task<DateTime> GetSyncDate(string rootURI)
        {
            DateTime syncDate;

            using (var client = new HttpClient())
            {
                client.Timeout = new TimeSpan(0, 0, 5);
                client.BaseAddress = new Uri(rootURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("v1/syncdate");
                if (response.IsSuccessStatusCode)
                {
                    String s = await response.Content.ReadAsStringAsync();
                    syncDate = DateTime.Parse(s.Trim('"'));
                }
                else
                {
                    throw new Exception();
                }
            }

            return syncDate;
        }

        public static async Task<Stream> GetUpdatedXML(string rootURI)
        {
            Stream fileData = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("v1/bookxml");
                if (response.IsSuccessStatusCode)
                {
                    fileData = await response.Content.ReadAsStreamAsync();
                }
                else
                {
                    String errorMsg = await response.Content.ReadAsStringAsync();
                    var responseMsg = JsonConvert.DeserializeObject<ResponseMessage>(errorMsg);

                    if (responseMsg != null)
                        throw new Exception("(#100 - Getting book xml)" + responseMsg.Message);

                    throw new Exception("(#100 - Getting book xml) Exception occured on sync");
                }
            }
            return fileData;
        }

        public static async Task<UpdatedSubSectionItem[]> GetSubSectionFileIDsUpdatedSince(string rootURI,DateTime? date)
        {
            UpdatedSubSectionItem[] updates = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                Uri uri;

                if (!date.HasValue)
                {
                    // first sync
                    uri = new Uri("v1/subsections?date", UriKind.RelativeOrAbsolute);
                }
                else
                {
                    // not first sync
                    uri = new Uri(string.Format("v1/subsections?date={0}", date.Value.ToString("yyyy-MM-dd HH:mm:ss")), UriKind.RelativeOrAbsolute);
                }

                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    updates = await response.Content.ReadAsAsync<UpdatedSubSectionItem[]>();
                }
                else
                {
                    String errorMsg = await response.Content.ReadAsStringAsync();
                    var responseMsg = JsonConvert.DeserializeObject<ResponseMessage>(errorMsg);

                    if (responseMsg != null)
                        throw new Exception("(#101 - Getting SubSection IDs)" + responseMsg.Message);

                    throw new Exception("(#101 - Getting SubSection IDs) Exception occured on sync");
                }
            }
            return updates;
        }

        public static async Task<Stream> GetUpdatedSubSectionFiles(string rootURI, int[] filesToGet)
        {
            Stream fileData = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				HttpResponseMessage response = await client.PostAsJsonAsync ("v1/subsections", filesToGet);
                if (response.IsSuccessStatusCode)
                {
                    fileData = await response.Content.ReadAsStreamAsync();
                }
                else
                {
                    String errorMsg = await response.Content.ReadAsStringAsync();
                    var responseMsg = JsonConvert.DeserializeObject<ResponseMessage>(errorMsg);

                    if (responseMsg != null)
                        throw new Exception("(#102 - Getting Sub-Section files)" + responseMsg.Message);

                    throw new Exception("(#102 - Getting Sub-Section files) Exception occured on sync");
                }
            }
            return fileData;
        }

        public static async Task<int[]> GetIndexFileIDsUpdatedSince(string rootURI, DateTime date)
        {
            int[] updates = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var uri = new Uri(string.Format("v1/indexes?date={0}", date.ToString("yyyy-MM-dd HH:mm:ss")), UriKind.RelativeOrAbsolute);
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    updates = await response.Content.ReadAsAsync<int[]>();
                }
                else
                {
                    String errorMsg = await response.Content.ReadAsStringAsync();
                    var responseMsg = JsonConvert.DeserializeObject<ResponseMessage>(errorMsg);

                    if (responseMsg != null)
                        throw new Exception("(#103 - Getting index file IDs)" + responseMsg.Message);

                    throw new Exception("(#103 - Getting index file IDs) Exception occured on sync");
                }
            }
            return updates;
        }

        public static async Task<Stream> GetUpdatedIndexFiles(string rootURI, int[] filesToGet)
        {
            Stream fileData = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.PostAsJsonAsync("v1/indexes", filesToGet);
                if (response.IsSuccessStatusCode)
                {
                    fileData = await response.Content.ReadAsStreamAsync();
                }
                else
                {
                    String errorMsg = await response.Content.ReadAsStringAsync();
                    var responseMsg = JsonConvert.DeserializeObject<ResponseMessage>(errorMsg);

                    if (responseMsg != null)
                        throw new Exception("(#104 - Getting index files)" + responseMsg.Message);

                    throw new Exception("(#104 - Getting index files) Exception occured on sync");
                }
            }
            return fileData;
        }
    }
}
