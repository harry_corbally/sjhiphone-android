﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedModel.Model
{
    public class UpdatedSubSectionItem
    {
        public Int32? OriginalID { get; set; }
        public Int32 ActualID { get; set; }
    }
}
