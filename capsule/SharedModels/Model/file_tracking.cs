using System;
using System.Collections.Generic;

namespace SharedModel.Model
{
    public partial class file_tracking
    {
        public int id { get; set; }
        public string file_name { get; set; }
        public DateTime last_updated_date { get; set; }
        public string file_type { get; set; }
    }

    public enum FileType { HTML, INDEX, XML, Image };
}
