﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SharedModel.Model
{
    [DebuggerDisplay("{Word}, {Occurences.Count}")]
    public class SearchResultItem
    {
        public SearchResultItem()
        {
            Occurences = new List<Occurence>();
        }

        public String Word { get; set; }
        public List<Occurence> Occurences { get; set; }
    }

    public class Occurence
    {
        public Int32 GroupId { get; set; }
        public String GroupTitle { get; set; }
        public Int32 BookId { get; set; }
        public String BookTitle { get; set; }
        public Int32 ChapterId { get; set; }
        public String ChapterTitle { get; set; }
        public Int32 SectionId { get; set; }
        public String SectionTitle { get; set; }
        public Int32 SubSectionId { get; set; }
        public String SubSectionTitle { get; set; }

        public Boolean HasSubSection
        {
            get
            {
                return !String.IsNullOrWhiteSpace(SubSectionTitle);
            }
        }

        public String ResultText
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0} -> ", ChapterTitle);
                if (!String.IsNullOrWhiteSpace(SubSectionTitle))
                    sb.AppendFormat("{0} -> {1}", SectionTitle, SubSectionTitle);
                else
                    sb.Append(SectionTitle);

                return sb.ToString();
            }
        }
    }

    public class OccurenceComparer : IEqualityComparer<Occurence>
    {
        public bool Equals(Occurence x, Occurence y)
        {
            return x.SubSectionId == y.SubSectionId;
        }

        public int GetHashCode(Occurence obj)
        {
            return obj.SubSectionId.GetHashCode();
        }
    }

}
