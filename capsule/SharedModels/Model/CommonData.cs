﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Collections.ObjectModel;

namespace SharedModel.Model
{
    public interface ParentCommonData<T> : ICommonData
    {
         ObservableCollection<T> Items {get;set;}
    }

    public interface ICommonData
    {
         String Colour { get; set; }

         String Alert { get; set; }

         String Folder { get; set; }

         Int32 Id { get; set; }

         String Title { get; set; }

         String Subtitle { get; set; }

         String Description { get; set; }

         Uri Image { get; set; }

         string ImageString { get;set; }

         byte[] ImageBytes { get; set; }
    }
}
