﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SharedModel.Model
{
    [DebuggerDisplay("BookGroup_ID = {Id}")]
    public class BookGroup : ObservableObject, ParentCommonData<BookItem>
    {
        public const String ItemsPropertyName = "Items";

        private ObservableCollection<BookItem> _items = new ObservableCollection<BookItem>();
        public ObservableCollection<BookItem> Items
        {
            get { return _items; }
            set { Set(ItemsPropertyName, ref _items, value); }
        }

        public string url { get; set; }
        public bool IsAlertNotificationGroup { get; set; }


        internal static Uri _baseUri = new Uri("ms-appdata:///");
        public const String colourPropertyName = "Colour";

        private String _colour;
        public String Colour
        {
            get { return _colour; }
            set { Set(colourPropertyName, ref _colour, value); }
        }

        public const String AlertPropertyName = "Alert";

        private String _alert;
        public String Alert
        {
            get { return _alert; }
            set { Set(AlertPropertyName, ref _alert, value); }
        }

        public const String FolderPropertyName = "Folder";

        private String _folder;
        public String Folder
        {
            get { return _folder; }
            set { Set(FolderPropertyName, ref _folder, value); }
        }

        /// <summary>
        /// The <see cref="Id"/> property's name
        /// </summary>
        public const String IdPropertyName = "Id";

        private Int32 _id;
        public Int32 Id
        {
            get { return _id; }
            set { Set(IdPropertyName, ref _id, value); }
        }

        /// <summary>
        /// The <see cref="Title"/> property's name
        /// </summary>
        public const String TitlePropertyName = "Title";

        private String _title;
        public String Title
        {
            get { return _title; }
            set { Set(TitlePropertyName, ref _title, value); }
        }

        /// <summary>
        /// The <see cref="Subtitle"/> property's name.
        /// </summary>
        public const String SubTitlePropertyName = "Subtitle";

        private String _subtitle;
        public String Subtitle
        {
            get { return _subtitle; }
            set { Set(SubTitlePropertyName, ref _subtitle, value); }
        }

        public const String DescriptionPropertyName = "Description";

        private String _description;
        public String Description
        {
            get { return _description; }
            set { Set(DescriptionPropertyName, ref _description, value); }
        }

        public const String ImagePropertyName = "Image";

        private Uri _image;
        public Uri Image
        {
            get { return _image; }
            set { Set(ImagePropertyName, ref _image, value); }
        }

        public string ImageString
        {
            get
            {
                if (Image == null)
                    return "";
                return Image.ToString();
            }
            set
            {
                try
                {
                    //Image = new Uri(value);
                }
                finally { }
            }
        }

        public Byte[] ImageBytes
        {
            get
            {
                return null;
            }
            set
            {
                try
                {
                }
                finally { }
            }
        }
    }
}
