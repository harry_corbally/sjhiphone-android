﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
//using Windows.Storage;
//using Windows.Storage.AccessCache;
using System.Diagnostics;

namespace SharedModel.Model
{
    [DebuggerDisplay("SectionItem_ID = {Id}")]
    public class SectionItem : ObservableObject, ParentCommonData<SubSectionItem>
    {
        public object Parent { get; set; }

        public const String ItemsPropertyName = "Items";

        private ObservableCollection<SubSectionItem> _items = new ObservableCollection<SubSectionItem>();
        public ObservableCollection<SubSectionItem> Items
        {
            get { return _items; }
            set { Set(ItemsPropertyName, ref _items, value); }
        }

        public string absolutepath = "";
        internal static Uri _htmlBaseUri = new Uri("ms-appx-web:///");

        public ChapterItem ParentItem
        {
            get
            {
                return (ChapterItem)Parent;
            }
            set
            {
                Parent = value;
            }
        }


        private const String HtmlPageUriPropertyName = "HtmlPageUri";

        private Uri _htmlPageUri;
        public Uri HtmlPageUri
        {
            get
            {
                return _htmlPageUri;
            }
        }

        public void SetHtmlPageUri(String sectionFolderName)
        {
            if (!String.IsNullOrEmpty(_htmlPath))
            {
                _htmlPageUri = new Uri(_baseUri, String.Format("local\\{0}\\{1}\\{2}\\{3}", "CapsuleMainFolder", "Content", sectionFolderName, _htmlPath));
            }
        }

        private String _htmlPath;
        public String HtmlPath
        {
            get { return _htmlPath; }
            set { Set(HtmlPageUriPropertyName, ref _htmlPath, value); }
        }

        //public async Task<Uri> GetHtmlPageUriAsync()
        //{
        //    StorageFolder folder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync(this.Id);

        //    Uri pageUri = new Uri("ms-appx-web://" + folder.FolderRelativeId + _htmlPath);

        //    return pageUri;
        //}

        public string SectionStatus { get; set; }

        public int ChapterId { get; set; }

        public string url { get; set; }

        public int sharepoint_list_id { get; set; }

        public int sequence { get; set; }

        public string meta_keywords { get; set; }

        public string modified_by { get; set; }

        public string modified_by_name { get; set; }

        public DateTime? modified_date { get; set; }

        public string created_by { get; set; }

        public string created_by_name { get; set; }

        public DateTime? created_date { get; set; }

        public int? last_published_section_id { get; set; }

        public Boolean is_published { get; set; }

        public DateTime? first_publish_date { get; set; }

        internal static Uri _baseUri = new Uri("ms-appdata:///");
        public const String colourPropertyName = "Colour";

        private String _colour;
        public String Colour
        {
            get { return _colour; }
            set { Set(colourPropertyName, ref _colour, value); }
        }

        public const String AlertPropertyName = "Alert";

        private String _alert;
        public String Alert
        {
            get { return _alert; }
            set { Set(AlertPropertyName, ref _alert, value); }
        }

        public const String FolderPropertyName = "Folder";

        private String _folder;
        public String Folder
        {
            get { return _folder; }
            set { Set(FolderPropertyName, ref _folder, value); }
        }

        /// <summary>
        /// The <see cref="Id"/> property's name
        /// </summary>
        public const String IdPropertyName = "Id";

        private Int32 _id;
        public Int32 Id
        {
            get { return _id; }
            set { Set(IdPropertyName, ref _id, value); }
        }

        /// <summary>
        /// The <see cref="Title"/> property's name
        /// </summary>
        public const String TitlePropertyName = "Title";

        private String _title;
        public String Title
        {
            get { return _title; }
            set { Set(TitlePropertyName, ref _title, value); }
        }

        /// <summary>
        /// The <see cref="Subtitle"/> property's name.
        /// </summary>
        public const String SubTitlePropertyName = "Subtitle";

        private String _subtitle;
        public String Subtitle
        {
            get { return _subtitle; }
            set { Set(SubTitlePropertyName, ref _subtitle, value); }
        }

        public const String DescriptionPropertyName = "Description";

        private String _description;
        public String Description
        {
            get { return _description; }
            set { Set(DescriptionPropertyName, ref _description, value); }
        }

        public const String ImagePropertyName = "Image";

        private Uri _image;
        public Uri Image
        {
            get { return _image; }
            set { Set(ImagePropertyName, ref _image, value); }
        }

        private string _imageString;
        public string ImageString
        {
            get
            {
                return _imageString;
            }
            set
            {
                try
                {
                    _imageString = value;
                }
                finally { }
            }
        }

        public Byte[] ImageBytes
        {
            get
            {
                return null;
            }
            set
            {
                try
                {
                }
                finally { }
            }
        }            
    }
}
