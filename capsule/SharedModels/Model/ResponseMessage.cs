﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedModel.Model
{
    public class ResponseMessage
    {
        public String Message { get; set; }
    }
}
