﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedModel.Model
{
    public class SectionBookGroupIdsView
    {
        public int subsection_id { get; set; }
        public int book_id { get; set; }
        public int group_id { get; set; }
    }
}
