﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedModel.Model
{
    public class SyncResult
    {
        public Boolean HasError { get; set; }
        public DateTime SyncDate { get; set; }
    }
}
