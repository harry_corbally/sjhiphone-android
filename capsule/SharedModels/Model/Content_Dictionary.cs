using System;
using System.Collections.Generic;
using SharedModel.Model;

namespace SharedModel.Model
{
    public partial class Content_Dictionary
    {
        public int id { get; set; }
        public string prefix { get; set; }
        public string word { get; set; }
        public int subsection_id { get; set; }
        public virtual SubSectionItem SubSection { get; set; }
    }
}
