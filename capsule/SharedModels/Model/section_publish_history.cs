using System;
using System.Collections.Generic;
using SharedModel.Model;

namespace SharedModel.Model
{
    public partial class section_publish_history
    {
        public int id { get; set; }
        public Nullable<int> section_id { get; set; }
        public Nullable<System.DateTime> approval_request_date { get; set; }
        public string user_requesting_approaval { get; set; }
        public string request_to_approver_user { get; set; }
        public string approval_user { get; set; }
        public string approval_outcome { get; set; }
        public Nullable<System.DateTime> approval_outcome_date { get; set; }
        public virtual SectionItem Section { get; set; }
        public string change_description { get; set; }
        public string user_requesting_approval_name { get; set; }
        public string approval_user_name { get; set; }
    }
}
