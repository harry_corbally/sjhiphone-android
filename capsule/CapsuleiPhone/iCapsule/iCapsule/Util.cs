﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iCapsule
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	public partial class Util : UIApplicationDelegate
	{

		public static bool IsiOS7 {
			get { return UIDevice.CurrentDevice.CheckSystemVersion (7, 0); }
		}

	}

	public static class Extensions
	{
		public static void LoadRequestWithSearch (this UIWebView webView, NSUrlRequest request)
		{
			webView.LoadRequest (request);
		}

		public static Int32 HighlightAllOccurencesOfString(this UIWebView webView, String keywordToSearch)
		{
			if (webView.Tag != 100) {
				String path = System.IO.Path.Combine (NSBundle.MainBundle.BundlePath, "Snapshot_IsoStore", "CapsuleMainFolder", "Content", @"CapsuleScript.js");
				String jsCode = System.IO.File.ReadAllText (path);
				webView.EvaluateJavascript (jsCode);

				webView.Tag = 100;
			}

			String startSearch = String.Format(@"heighlightText('{0}')", keywordToSearch);
			webView.EvaluateJavascript(startSearch);

			String result = webView.EvaluateJavascript(@"heighLightedTexts.length");
			return Convert.ToInt32(result);
		}

		public static void RemoveAllHighlights(this UIWebView webView)
		{
			webView.EvaluateJavascript (@"MyApp_RemoveAllHighlights()");
		}

		public static Int32 HighlightNext(this UIWebView webView)
		{
			webView.EvaluateJavascript (@"next()");
			return webView.HighlightedPosition ();
		}

		public static Int32 HighlightPrevious(this UIWebView webView)
		{
			webView.EvaluateJavascript (@"previous()");
			return webView.HighlightedPosition ();
		}

		/// <summary>
		/// Zero based highlighted text index
		/// </summary>
		/// <param name="webView">Web view.</param>
		public static Int32 HighlightedPosition(this UIWebView webView)
		{
			return Convert.ToInt32(webView.EvaluateJavascript (@"i"));
		}
	}
}

