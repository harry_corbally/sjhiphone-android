// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;

namespace iCapsule
{
	[Register ("iCapsuleViewController")]
	partial class iCapsuleViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		MonoTouch.UIKit.UIBarButtonItem btnNext { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		MonoTouch.UIKit.UIBarButtonItem btnPrevious { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		MonoTouch.UIKit.UISearchBar searchBar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		MonoTouch.UIKit.UIWebView sectionWebView { get; set; }

		[Action ("next:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void next (MonoTouch.UIKit.UIBarButtonItem sender);

		[Action ("previous:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void previous (MonoTouch.UIKit.UIBarButtonItem sender);

		[Action ("searchItemClicked:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void searchItemClicked (MonoTouch.UIKit.UIBarButtonItem sender);

		void ReleaseDesignerOutlets ()
		{
			if (btnNext != null) {
				btnNext.Dispose ();
				btnNext = null;
			}
			if (btnPrevious != null) {
				btnPrevious.Dispose ();
				btnPrevious = null;
			}
			if (searchBar != null) {
				searchBar.Dispose ();
				searchBar = null;
			}
			if (sectionWebView != null) {
				sectionWebView.Dispose ();
				sectionWebView = null;
			}
		}
	}
}
