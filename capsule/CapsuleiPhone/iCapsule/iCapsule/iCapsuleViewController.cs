﻿using System;
using System.Drawing;
using System.IO;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using SharedModel.Model;
using System.CodeDom.Compiler;

namespace iCapsule
{
	public partial class iCapsuleViewController : UIViewController
	{
		public SectionItem SelectedSection;
		public SubSectionItem SelectedSubSection;
		private Int32 highlightedTexts;
		private Int32 _currentPosition;
		public iCapsuleViewController (IntPtr handle) : base (handle)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		#region View lifecycle

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			if (this.SelectedSection != null) {
				this.Title = this.SelectedSection.Title;
				string fileName = this.SelectedSection.HtmlPath; // remember case-sensitive
				string localHtmlUrl = Path.Combine ("file://", NSBundle.MainBundle.BundlePath, "Snapshot_IsoStore", this.SelectedSection.absolutepath, fileName);
				//string localHtmlUrl = @"Page1.html";
				localHtmlUrl = localHtmlUrl.Replace (@"\", @"/");
				this.sectionWebView.LoadRequestWithSearch (new NSUrlRequest (new NSUrl (localHtmlUrl, false)));
			} else if (this.SelectedSubSection != null) {
				this.Title = this.SelectedSubSection.Title;
				string fileName = this.SelectedSubSection.HtmlPath; // remember case-sensitive
				string localHtmlUrl = Path.Combine ("file://", NSBundle.MainBundle.BundlePath, "Snapshot_IsoStore", this.SelectedSubSection.absolutepath, fileName);
				localHtmlUrl = localHtmlUrl.Replace (@"\", @"/");
				this.sectionWebView.LoadRequest (new NSUrlRequest (new NSUrl (localHtmlUrl, false)));
			}
			this.searchBar.Delegate = new SearchBarDelegate (this);
			this.EnableDisableButtons ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			this.NavigationController.SetToolbarHidden (false, true);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			this.NavigationController.SetToolbarHidden (true, true);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}

		#endregion

		partial void searchItemClicked (UIBarButtonItem sender)
		{
			if (this.searchBar.Hidden)
			{
				UIBarButtonItem rightItem = new UIBarButtonItem(UIBarButtonSystemItem.Refresh);
				rightItem.Style = UIBarButtonItemStyle.Bordered;
				rightItem.Clicked += (sender1, e) => { searchItemClicked((UIBarButtonItem)sender1); };
				this.NavigationItem.SetRightBarButtonItem(rightItem, true);

				UIView.Animate(0.5, 0, UIViewAnimationOptions.CurveEaseIn, () => {
					RectangleF wFrame = this.sectionWebView.Frame;
					wFrame.Height = wFrame.Height - this.searchBar.Frame.Height;
					wFrame.Y = wFrame.Location.Y + this.searchBar.Frame.Height;
					this.sectionWebView.Frame = wFrame;
				}, () => {
					this.searchBar.Hidden = false;
					this.searchBar.BecomeFirstResponder();
				});
			}
			else {
				UIBarButtonItem rightItem = new UIBarButtonItem(UIBarButtonSystemItem.Search);
				rightItem.Clicked += (sender1, e) => { searchItemClicked((UIBarButtonItem)sender1); };
				this.NavigationItem.SetRightBarButtonItem(rightItem, true);

				this.searchBar.ResignFirstResponder();
				this.sectionWebView.RemoveAllHighlights();

				UIView.Animate(0.5, 0, UIViewAnimationOptions.CurveEaseIn, () => {
					this.searchBar.Hidden = true;
					RectangleF wFrame = this.sectionWebView.Frame;
					wFrame.Height = wFrame.Height + this.searchBar.Frame.Height;
					wFrame.Y = wFrame.Location.Y - this.searchBar.Frame.Height;
					this.sectionWebView.Frame = wFrame;
				}, null);
			}
		}

		partial void next (UIBarButtonItem sender)
		{
			_currentPosition = this.sectionWebView.HighlightNext();
			this.EnableDisableButtons();
		}

		partial void previous (UIBarButtonItem sender)
		{
			_currentPosition = this.sectionWebView.HighlightPrevious();
			this.EnableDisableButtons();
		}

		private void EnableDisableButtons()
		{
			if (this.highlightedTexts > 1) {
				this.btnNext.Enabled = this.btnPrevious.Enabled = true;
				//this.btnNext.Enabled = (this._currentPosition < (this.highlightedTexts));
				//this.btnPrevious.Enabled = (this._currentPosition > 1);
			} else {
				this.btnNext.Enabled = false;
				this.btnPrevious.Enabled = false;
			}
		}

		class SearchBarDelegate : UISearchBarDelegate
		{
			private iCapsuleViewController controller;

			public SearchBarDelegate (iCapsuleViewController controller)
			{
				this.controller = controller;
			}

			public override void SearchButtonClicked (UISearchBar searchBar)
			{
				searchBar.ResignFirstResponder ();
				this.controller.highlightedTexts = this.controller.sectionWebView.HighlightAllOccurencesOfString (searchBar.Text);
				this.controller._currentPosition = this.controller.sectionWebView.HighlightedPosition ();
				this.controller.EnableDisableButtons();
			}

			public override void CancelButtonClicked (UISearchBar searchBar)
			{
				searchBar.ResignFirstResponder ();
			}
		}
	}
}
