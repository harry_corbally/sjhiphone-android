// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iCapsule
{
	[Register ("BookDetailViewController")]
	partial class BookDetailViewController
	{
		[Outlet]
		MonoTouch.UIKit.UISearchDisplayController get_searchDisplayController { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (get_searchDisplayController != null) {
				get_searchDisplayController.Dispose ();
				get_searchDisplayController = null;
			}
		}
	}
}
