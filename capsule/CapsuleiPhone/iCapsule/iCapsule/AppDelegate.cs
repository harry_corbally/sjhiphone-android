﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using SharedModel.Model;

namespace iCapsule
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		public static String RootDataFolder { get { return "Snapshot_IsoStore"; } }
		public static String XmlFileName { get { return "CapsuleBooks.xml"; } }
		public static String CapsuleMainFolderName { get { return "CapsuleMainFolder"; } }
		public static String SyncFolderName { get { return "SyncFolder"; } }

		public IEnumerable<BookGroup> BookGroups { get; set; }

		// class-level declarations
		public override UIWindow Window {
			get;
			set;
		}

		public override bool FinishedLaunching (UIApplication application, NSDictionary launchOptions)
		{
			UINavigationBar.Appearance.BackgroundColor = UIColor.FromRGBA (76, 95, 147, 255);
			//UINavigationBar.Appearance.TintColor = UIColor.White;
			//UINavigationBar.Appearance.BarTintColor = UIColor.FromRGBA (95, 119, 183, 255);
			UINavigationBar.Appearance.SetTitleTextAttributes (new UITextAttributes (){ TextColor = UIColor.White});

			if(Util.IsiOS7) {
				UINavigationBar.Appearance.TintColor = UIColor.White;
				UINavigationBar.Appearance.BarTintColor = UIColor.FromRGBA (95, 119, 183, 255);
			}
			else{
				UINavigationBar.Appearance.TintColor = UIColor.FromRGBA (95, 119, 183, 255);
				//UINavigationBar.Appearance.BarTintColor = UIColor.FromRGBA (95, 119, 183, 255);
			}

			return true;
		}

		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
		}
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
		}
		// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
		}
		// This method is called when the application is about to terminate. Save data, if needed.
		public override void WillTerminate (UIApplication application)
		{
		}


	}
}

