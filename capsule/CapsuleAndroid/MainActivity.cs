﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Support.V7.App;
//using Android.Support.V7.Widget;
using System.Collections.Generic;
using Android.Util;
using Android.Views;
using Android.Widget;
using SharedModel.Model;
using SharedModel.Services;
using CapsuleAndroid.CapsuleAdapters;

namespace CapsuleAndroid
{
	[Activity (Label = "Capsule", MainLauncher = true, Name = "capsuleandroid.MainActivity")]
	//[MetaData ("android.support.PARENT_ACTIVITY", Value = "capsuleandroid.MainActivity")]
	public class MainActivity : ActionBarActivity
	{
		public BookItem SelectedBook { get; set; }
		public SectionItem SelectedSection { get; set; }
		internal Android.Support.V7.Widget.SearchView _searchView;
		internal ListView _groupListView;
		internal GroupSectionAdapter _groupAdapter;

		async protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			//this.RequestWindowFeature (WindowFeatures.NoTitle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			this.ActionBar.SetDisplayHomeAsUpEnabled (false);

			_groupListView = FindViewById<ListView> (Resource.Id.lvGroups);
			_groupAdapter = new GroupSectionAdapter (this);

			var type = Intent.GetStringExtra ("type");
			if (type == "SectionItem") {
				SelectedSection = ((AppDelegate)Application).SelectedSection;
			} else {
				if (type == "BookItem") {
					SelectedBook = ((AppDelegate)Application).SelectedBook;
				}
			}
			if (SelectedSection != null) {
				this.ActionBar.SetDisplayHomeAsUpEnabled (true);
				this.Title = SelectedSection.Title;
				_groupAdapter.AddSection (SelectedSection.Title, SelectedSection.Image,
					new SectionAdapter (this, Resource.Layout.Item, SelectedSection.Items.ToList ()));
			} else if (SelectedBook != null) {
				this.ActionBar.SetDisplayHomeAsUpEnabled (true);
				this.Title = SelectedBook.Title;
				foreach (var chapter in SelectedBook.Items) {
					_groupAdapter.AddSection (chapter.Title, chapter.Image,
						new BookAdapter (this, Resource.Layout.Item, chapter.Items.ToList ()));
				}
			} else {
				this.Title = "Groups";
				_groupAdapter = await Load (_groupAdapter);
			}
			_groupListView.Adapter = _groupAdapter;
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			this.MenuInflater.Inflate (Resource.Menu.main_menu, menu);
			var syncItem = menu.FindItem (Resource.Id.action_synchronize);
			syncItem.SetVisible (false);
			var item = menu.FindItem(Resource.Id.action_search);
			// search is only available on Books screen
			if (this.SelectedSection == null && this.SelectedBook == null) {
				syncItem.SetVisible (true);
			}
			if (this.SelectedBook == null) {
				item.SetVisible (false);
			} else {
				var searchItem = MenuItemCompat.GetActionView (item);
				_searchView = searchItem.JavaCast<Android.Support.V7.Widget.SearchView> ();

				/*_searchView.QueryTextChange += (s, e) => {
					if (String.IsNullOrWhiteSpace(e.NewText)) {
						_groupListView.Adapter = _groupAdapter;
					}
				};*/
				var actionExpand = new OnActionExpandListener();            
				actionExpand.MenuItemCollaspe += (sender, e) =>
				{
					_groupListView.Adapter = _groupAdapter;
					e.Handled = true;
				};
				actionExpand.MenuItemActionExpand += (sender, e) =>
				{
					e.Handled = true;
				};

				Android.Support.V4.View.MenuItemCompat.SetOnActionExpandListener(item, actionExpand);

				_searchView.QueryTextSubmit += async (s, e) => {
					e.Handled = true;
					ProgressDialog dialog = new ProgressDialog(this);
					dialog.Indeterminate = true;
					dialog.SetProgressStyle(ProgressDialogStyle.Spinner);
					dialog.SetMessage("Searching...");
					dialog.Show();
					((Android.Support.V7.Widget.SearchView)s).ClearFocus();
					//TODO: Do something fancy when search button on keyboard is pressed
					var searchText = e.Query;
					if (!String.IsNullOrWhiteSpace(searchText)) {
						var _searchResult = new List<Occurence>();

					DirectoryInfo di = new DirectoryInfo(Path.Combine(AppDelegate.RootDataFolder, AppDelegate.SyncFolderName));
					var fi = di.GetFiles(String.Format("{0}*.index", searchText.Substring(0,1))).FirstOrDefault();
					using(var indxFile = new StreamReader(fi.FullName)) {
						var allWords = await CommonServices.ParseIndexFile(await indxFile.ReadToEndAsync(), AppDelegate.BookGroups);
						await Task.Run(() => {
							var res = allWords.Where(p => p.Word.ToLower().Contains(searchText.ToLower()));
							
							var x = res.SelectMany(p => p.Occurences)
								.Where(p => p.BookId == this.SelectedBook.Id)
								.OrderBy(p => p.BookTitle)
								.ThenBy(p => p.ChapterTitle)
								.ThenBy(p => p.SectionTitle)
								.ThenBy(p => p.SubSectionTitle)
								.GroupBy(p => p.SubSectionId)
									.ToList();

							foreach (var occurance in x)
							{
								_searchResult.Add(occurance.First());
							}
						});
							_groupListView.Adapter = new SearchAdapter(this, Resource.Layout.Item, _searchResult);
						}
					} else {
						_groupListView.Adapter = _groupAdapter;
					}
					dialog.Dismiss();
				};
			}
			return base.OnCreateOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Android.Resource.Id.Home:
				//Android.Support.V4.App.NavUtils.NavigateUpFromSameTask (this);
				Finish ();
			return true;
			case Resource.Id.action_search:
				return false;
			case Resource.Id.action_synchronize:
				var i = new Intent (this, typeof(SynchronizeActivity));
				this.StartActivityForResult (i, 0);
				return true;
			}
			return base.OnOptionsItemSelected (item);
		}

		async protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			if (resultCode == Result.Ok) {
				_groupAdapter = new GroupSectionAdapter (this);
				_groupAdapter = await Load (_groupAdapter);
				_groupListView.Adapter = _groupAdapter;
			}
		}

		/*
		[Java.Interop.Export("Info_Click")]
		public void Info_Click(View view)
		{
			var alert = new Dialog(this);
			alert.SetContentView(Resource.Layout.Overview);

			alert.FindViewById<TextView> (Resource.Id.txtOverview).Text = "test text will get and set later";

			alert.Show();
		}*/

		private async Task<GroupSectionAdapter> Load(GroupSectionAdapter groupAdapter) {
			this.ActionBar.Subtitle = "Loading please wait...";

			var xmlFile = new FileInfo (Path.Combine (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal), AppDelegate.RootDataFolder, AppDelegate.CapsuleMainFolderName, AppDelegate.XmlFileName));

			if (xmlFile.Exists) {
				using (StreamReader sr = new StreamReader (xmlFile.FullName)) {
					var content = await sr.ReadToEndAsync ();
					await Task.Run (() => {
						var groups = CommonServices.ParseXml (content,
							Path.Combine(xmlFile.DirectoryName, "Content")).ToList();

						var notiItem = groups.FirstOrDefault(x => x.IsAlertNotificationGroup);
						if (notiItem != null) {
							groups.Remove(notiItem);
							groups.Insert(0, notiItem);
						}

						foreach (var group in groups) {
							group.Image = new Uri (group.Image.AbsolutePath.Replace ("%5C", "/").Replace (@"\", @"/"));
							groupAdapter.AddSection (group.Title, group.Image,
								new GroupAdapter (this, Resource.Layout.Item, group.Items.ToList ()));

							foreach (var book in group.Items) {
								book.Parent = null;
								book.ParentItem = null;
								book.Image = new Uri (book.Image.AbsolutePath.Replace ("%5C", "/").Replace (@"\", @"/"));
								foreach (var chap in book.Items) {
									chap.Parent = null;
									chap.ParentItem = null;
									chap.Image = new Uri (chap.Image.AbsolutePath.Replace ("%5C", "/").Replace (@"\", @"/"));
									foreach (var sec in chap.Items) {
										sec.Parent = null;
										sec.ParentItem = null;
										sec.Image = new Uri (sec.Image.AbsolutePath.Replace ("%5C", "/").Replace (@"\", @"/"));
										foreach (var ssec in sec.Items) {
											ssec.Parent = null;
											ssec.ParentItem = null;
											ssec.Image = new Uri (ssec.Image.AbsolutePath.Replace ("%5C", "/").Replace (@"\", @"/"));
										}
									}
								}
							}
						}
						AppDelegate.BookGroups = groups;
					});
				}
				this.ActionBar.Subtitle = null;
			} else {
				this.ActionBar.Subtitle = "Book xml file doesn't exists.";
			}
			return groupAdapter;
		}
	}
}