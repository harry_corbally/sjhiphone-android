﻿using System;
using Android.App;
using Android.OS;
using Android.Webkit;
using Android.Views;
using Android.Widget;
using Android.Content;
using Android.Views.InputMethods;

namespace CapsuleAndroid
{
	[Activity (Label = "HtmlPageActivity")]
	[MetaData ("android.support.PARENT_ACTIVITY", Value = "capsuleandroid.MainActivity")]	
	public class HtmlPageActivity : Activity
	{
		public String _url;
		WebView webView;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.HtmlPage);

			this.ActionBar.SetDisplayHomeAsUpEnabled (true);

			this.Title = Intent.GetStringExtra ("ItemTitle");
			this._url = Intent.GetStringExtra ("PageUrl");

			webView = FindViewById<WebView> (Resource.Id.webView);
			container = (LinearLayout)FindViewById (Resource.Id.layoutId);
			container.Visibility = ViewStates.Gone;
			webView.Settings.JavaScriptEnabled = true;
			webView.LoadUrl ("file:///" + this._url);

			//webView.FindAll ("");
			//webView.Settings.co
		}

		private Boolean isSearchDisplayed;
		public override bool OnKeyDown (Keycode keyCode, KeyEvent e)
		{
			if (keyCode == Keycode.Back) {
				OnBackPressed ();
				return true;
			} else if (keyCode == Keycode.Search) {
				return false;
			}
			return base.OnKeyDown (keyCode, e);
		}

		public override void OnBackPressed ()
		{
			if (isSearchDisplayed) {
				isSearchDisplayed = false;
				container.RemoveAllViews ();
				webView.ClearMatches ();
				return;
			}
			base.OnBackPressed ();
		}

		public override bool OnSearchRequested ()
		{
			search ();
			return base.OnSearchRequested ();
		}

		/*private const int SEARCH_MENU_ID = Menu.First;

		public override Boolean OnCreateOptionsMenu (IMenu menu)
		{
			base.OnCreateOptionsMenu (menu);
			menu.Add(0, SEARCH_MENU_ID, 0, "Search");

			return true;
		}

		public override Boolean OnPrepareOptionsMenu(IMenu menu){  
			base.OnPrepareOptionsMenu(menu);
			return true;
		}*/

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Android.Resource.Id.Home:
				Finish ();
				return true;
				/*case SEARCH_MENU_ID:
				search ();
				return true;*/
			}
			return base.OnOptionsItemSelected (item);
		}

		private LinearLayout container;
		private ImageButton nextButton, prevButton;
		private EditText findBox;

		public void search() {
			if (!isSearchDisplayed) {
				isSearchDisplayed = true;
				container.Visibility = ViewStates.Visible;

				prevButton = new ImageButton (this);
				//prevButton.Text = "Previous";
				prevButton.SetBackgroundResource(Resource.Drawable.up_button);	//"@drawable/up_button";
				prevButton.Click += (sender, e) => {
					webView.FindNext (false);
				};
				prevButton.Visibility = ViewStates.Gone;
				container.AddView (prevButton);

				nextButton = new ImageButton (this);
				nextButton.SetBackgroundResource(Resource.Drawable.down_button);	//"@drawable/down_button";
				//nextButton.Text = "Next";
				nextButton.Click += (sender, e) => {
					webView.FindNext (true);
				};
				nextButton.Visibility = ViewStates.Gone;
				container.AddView (nextButton);

				findBox = new EditText (this);
				findBox.ImeOptions = ImeAction.Search;
				findBox.SetMinEms (30);
				findBox.SetSingleLine (true);
				findBox.Hint = "Search";
				findBox.KeyPress += (object sender, View.KeyEventArgs e) => {
					e.Handled = false;
					if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) {
						int itemsFound = webView.FindAll (findBox.Text.Trim ());
						if (itemsFound > 1) {
							nextButton.Visibility = ViewStates.Visible;
							prevButton.Visibility = ViewStates.Visible;
						} else {
							nextButton.Visibility = ViewStates.Gone;
							prevButton.Visibility = ViewStates.Gone;
						}
						InputMethodManager manager = (InputMethodManager) GetSystemService(InputMethodService);
						manager.HideSoftInputFromWindow(findBox.WindowToken, 0);

						try {
							/*var t = Class.FromType(webView.GetType());
						var m = t.GetMethod("setFindIsUp", typeof(Boolean));
						//Method m; //= WebView.Class.GetMethod("setFindIsUp", Boolean.TYPE);
						m.Invoke(webView, true);*/
						} catch (Exception ignored) {
						}
						e.Handled = true;
					}
				};
				container.AddView (findBox);
			}
		}
	}
}