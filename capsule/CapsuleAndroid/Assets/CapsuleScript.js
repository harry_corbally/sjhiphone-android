﻿$(document).ready(function () {

    var setTbodyRows = function (doc) {

        var headerTextsArr = [];

        doc.find('table').each(function () {

            var $table = $(this);

            $table.find('thead > tr > th').each(function () {
                var x = $(this).text();
                headerTextsArr.push(x);
            });

            $table.find('tbody > tr').each(function () {

                $row = $(this);

                var idx = 0;

                $row.find('td').each(function () {
                    var $td = $(this);

                    var header = headerTextsArr[idx++];

                    $td.attr('data-title', header);
                });

            });

            headerTextsArr = [];

        });

    };

    setTbodyRows($(this));
	
	
	
	

});

var i = 0;

	var heighlightText = function (txt) {

		var options = {
			exact: "partial", keys: txt, style_name_suffix: false
		};

		$(function () {

			$("span").removeClass("hilite");
			$("span").removeClass("hiliteGreen");

			$(document).SearchHighlight(options);

			heighLightedTexts = $(".hilite");
			i = 0;

			if (heighLightedTexts !== undefined && heighLightedTexts.length > 0) {
				$(heighLightedTexts[0]).removeClass("hilite").addClass("hiliteGreen");
				i = 1;
			}
			else {
				window.external.Notify('noresult');
			}
		});
	}

	var heighLightedTexts;

	var next = function () {

		if (heighLightedTexts == undefined || heighLightedTexts.length == 0)
			heighLightedTexts = $(".hilite");

		if (i == heighLightedTexts.length) {
			$(heighLightedTexts[i - 1]).removeClass("hiliteGreen").addClass("hilite");
			i = 0;
		}

		if (i > 0)
			$(heighLightedTexts[i - 1]).removeClass("hiliteGreen").addClass("hilite");

		$(heighLightedTexts[i]).removeClass("hilite").addClass("hiliteGreen");
		$(heighLightedTexts[i]).focus();

		i++;

		//ScrollToHighlightedText();
	}

	var previous = function () {

		if (heighLightedTexts == undefined || heighLightedTexts.length == 0)
			heighLightedTexts = $(".hilite");

		if (i == 0) {
			$(heighLightedTexts[i]).removeClass("hiliteGreen").addClass("hilite");
			i = heighLightedTexts.length - 1;
			$(heighLightedTexts[i]).removeClass("hilite").addClass("hiliteGreen");
		}
		else if (i == heighLightedTexts.length - 1) {
			$(heighLightedTexts[i]).removeClass("hiliteGreen").addClass("hilite");
			i--;
			$(heighLightedTexts[i]).removeClass("hilite").addClass("hiliteGreen");
		}
		else {
			$(heighLightedTexts[i]).removeClass("hiliteGreen").addClass("hilite");
			i--;
			$(heighLightedTexts[i]).removeClass("hilite").addClass("hiliteGreen");
		}

		//ScrollToHighlightedText();
	}

	var ScrollToHighlightedText = function () {
		$('html, body').animate({
			scrollTop: $(".hiliteGreen").offset().top
		}, 0);
	}