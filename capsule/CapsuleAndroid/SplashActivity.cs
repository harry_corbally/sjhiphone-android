﻿
using System;

using Android.App;
using Android.OS;

namespace CapsuleAndroid
{
	[Activity (Theme = "@style/Theme.Splash", NoHistory = true)]			
	public class SplashActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			StartActivity (typeof(MainActivity));
		}
	}
}

