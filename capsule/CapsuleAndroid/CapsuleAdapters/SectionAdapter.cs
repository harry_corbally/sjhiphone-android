﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using SharedModel.Model;

namespace CapsuleAndroid.CapsuleAdapters
{
	public class SectionAdapter : BaseAdapter<SubSectionItem>
	{
		private Activity _context;  
		private List<SubSectionItem> _items;
		private int _templateResourceId;
		public SectionAdapter(Activity context, 
			int templateResourceId, List<SubSectionItem> items) : base()
		{
			_context = context;
			_templateResourceId = templateResourceId;
			_items = items;
		}
		public override int Count { get { return _items.Count; } }
		public override SubSectionItem this[int index] { get { return _items[index]; } }
		public override long GetItemId(int position) { return position; }
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			SubSectionItem item = this [position];
			View view = convertView;
			if (view == null || !(view is RelativeLayout)) {
				view = _context.LayoutInflater.Inflate (_templateResourceId, parent, false);
			}
			view.Tag = position;
			view.Click += (sender, e) => {
				var subSection = this[Convert.ToInt32(((RelativeLayout)sender).Tag)];
				var nextActivity = new Intent(this._context, typeof(HtmlPageActivity));
				nextActivity.PutExtra("ItemTitle", subSection.Title);
				nextActivity.PutExtra("PageUrl", Path.Combine(AppDelegate.RootDataFolder, subSection.absolutepath.Replace(@"\", @"/"), subSection.HtmlPath));
				_context.StartActivity(nextActivity);
			};
			var imgInfo = view.FindViewById<ImageButton> (Resource.Id.imgInfo);
			imgInfo.Visibility = ViewStates.Gone;

			TextView title = view.FindViewById<TextView> (Resource.Id.title);
			title.Text = item.Title;
			TextView subTitle = view.FindViewById<TextView> (Resource.Id.subTitle);
			subTitle.Text = item.Subtitle;
			var imgItem = view.FindViewById<ImageView> (Resource.Id.imgItem);
			try
			{
				using (var reader = new FileStream(item.Image.AbsolutePath, FileMode.Open, FileAccess.Read)) {
					imgItem.SetImageDrawable (BitmapDrawable.CreateFromStream (reader, null));
				}
			}
			catch
			{
				imgItem.Visibility = ViewStates.Gone;
			}
			return view;
		}
	}
}