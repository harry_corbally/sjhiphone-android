﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using SharedModel.Model;
using System.IO;

namespace CapsuleAndroid.CapsuleAdapters
{
	public class GroupAdapter : BaseAdapter<BookItem>
	{
		private Activity _context;  
		private List<BookItem> _items;
		private int _templateResourceId;
		public GroupAdapter(Activity context, 
			int templateResourceId, List<BookItem> items) : base()
		{
			_context = context;
			_templateResourceId = templateResourceId;
			_items = items;
		}
		public override int Count { get { return _items.Count; } }
		public override BookItem this[int index] { get { return _items[index]; } }
		public override long GetItemId(int position) { return position; }
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			BookItem item = this [position];
			View view = convertView;
			if (view == null || !(view is RelativeLayout)) {
				view = _context.LayoutInflater.Inflate (_templateResourceId, parent, false);
			}
			var imgInfo = view.FindViewById<ImageButton> (Resource.Id.imgInfo);
			// if book has chapters then attach click event
			if (item.Items.Count > 0) {
				view.Tag = position;
				view.Click += async (sender, e) => {
					((RelativeLayout)sender).Enabled = false;
					ProgressDialog dialog = new ProgressDialog (this._context);
					dialog.Indeterminate = true;
					dialog.SetProgressStyle (ProgressDialogStyle.Spinner);
					dialog.SetMessage ("Loading, please wait...");
					dialog.SetCancelable (false);
					dialog.Show ();
					System.Threading.Tasks.Task t = System.Threading.Tasks.Task.Run (() => {
						var nextActivity = new Intent (this._context, typeof(MainActivity));
						var objToSend = this [(Int32)((RelativeLayout)sender).Tag];

						((AppDelegate)this._context.Application).SelectedBook = objToSend;

						nextActivity.PutExtra ("type", "BookItem");	//, JsonConvert.SerializeObject (objToSend));
						_context.StartActivity (nextActivity);
					});
					await t;
					dialog.Dismiss ();
					((RelativeLayout)sender).Enabled = true;
				};
				imgInfo.Tag = position;
				imgInfo.Click += (object sender, EventArgs e) => {
					var objSelected = this [(Int32)((ImageButton)sender).Tag];
					var alert = new Dialog (this._context);
					alert.Window.RequestFeature(WindowFeatures.NoTitle);
					alert.Window.SetBackgroundDrawable(new ColorDrawable(Android.Graphics.Color.Transparent));
					alert.SetContentView (Resource.Layout.Overview);
					alert.SetCanceledOnTouchOutside(true);

					alert.FindViewById<TextView> (Resource.Id.txtOverview).Text = objSelected.Description;

					alert.Show ();
				};
			} else {
				view.Enabled = false;
				imgInfo.Visibility = ViewStates.Gone;
			}
			TextView title = view.FindViewById<TextView> (Resource.Id.title);
			title.Text = item.Title;
			TextView subTitle = view.FindViewById<TextView> (Resource.Id.subTitle);
			if (String.IsNullOrEmpty (item.Subtitle)) {
				subTitle.Visibility = ViewStates.Gone;
			} else {
				subTitle.Text = item.Subtitle;
			}
			var imgItem = view.FindViewById<ImageView> (Resource.Id.imgItem);
			try
			{
				using (var reader = new FileStream(item.Image.AbsolutePath, FileMode.Open, FileAccess.Read)) {
					imgItem.SetImageDrawable (BitmapDrawable.CreateFromStream (reader, null));
				}
			}
			catch
			{
				imgItem.Visibility = ViewStates.Gone;
			}
			return view;
		}
	}
}