﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;

namespace CapsuleAndroid.CapsuleAdapters
{
	public class GroupSectionAdapter : BaseAdapter<ListSection>
	{
		private const int TYPE_SECTION_HEADER = 0;
		private Context _context;
		private LayoutInflater _inflater;
		private List<ListSection> _sections; 
		public GroupSectionAdapter(Context context)
		{
			_context = context;
			_inflater = LayoutInflater.From(_context);
			_sections = new List<ListSection>();
		}
		public List<ListSection> Sections { get { return _sections; } set { _sections = value; } }

		// Each section has x list items + 1 list item for the caption. This is the reason for the +1 in the tally
		public override int Count
		{
			get
			{
				int count = 0;
				foreach (ListSection s in _sections) count += s.Adapter.Count + 1;
				return count;
			}
		}

		// We know there will be at least 1 type, the seperator, plus each
		// type for each section, that is why we start with 1
		public override int ViewTypeCount
		{
			get
			{
				int viewTypeCount = 1;
				foreach (ListSection s in _sections) viewTypeCount += s.Adapter.ViewTypeCount;
				return viewTypeCount;
			}
		} 
		public override ListSection this[int index] { get { return _sections[index]; } }
		// Since we dont want the captions selectable or clickable returning a hard false here achieves this
		public override bool AreAllItemsEnabled() { return false; } 
		public override int GetItemViewType(int position)
		{
			int typeOffset = TYPE_SECTION_HEADER + 1;
			foreach (ListSection s in _sections)
			{
				if (position == 0) return TYPE_SECTION_HEADER;
				int size = s.Adapter.Count + 1;
				if (position < size) return (typeOffset + s.Adapter.GetItemViewType(position - 1)); 
				position -= size;
				typeOffset += s.Adapter.ViewTypeCount;
			}
			return -1;
		}
		public override long GetItemId(int position) { return position; }
		public void AddSection(String caption, Uri image, BaseAdapter adapter)
		{
			_sections.Add(new ListSection(caption, image, adapter));
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;
			foreach (ListSection s in _sections)
			{
				// postion == 0 means we have to inflate the section separator
				if (position == 0) 
				{
					if (view == null || !(view is LinearLayout))
					{
						view = _inflater.Inflate(Resource.Layout.HeaderItem, parent, false);
					}
					view.Click += (sender, e) => { };
					TextView caption = view.FindViewById<TextView>(Resource.Id.caption);
					caption.Text = s.Caption;
					var imgItem = view.FindViewById<ImageView> (Resource.Id.imgItem);
					try
					{
						using (var reader = this._context.Assets.Open (s.Image.AbsolutePath.TrimStart('/'))) {
							imgItem.SetImageDrawable (BitmapDrawable.CreateFromStream (reader, null));
						}
					}
					catch
					{
						imgItem.Visibility = ViewStates.Gone;
					}
					return view;
				}
				int size = s.Adapter.Count + 1;
				// postion < size means we are at an item, so we just pass through its View from its adapter
				if (position < size) return s.Adapter.GetView(position - 1, convertView, parent);
				position -= size;
			}
			return null;
		}

		public override Java.Lang.Object GetItem(int position)
		{
			foreach (ListSection s in _sections)
			{
				if (position == 0) return null; // this is a separator item, dont want it instantiated
				int size = s.Adapter.Count + 1;
				if (position < size) return s.Adapter.GetItem(position - 1);
				position -= size;
			}
			return null;
		}
	}
}