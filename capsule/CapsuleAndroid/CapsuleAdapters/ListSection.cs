﻿using System;
using Android.Widget;

namespace CapsuleAndroid.CapsuleAdapters
{
	public class ListSection
	{
		private String _caption;
		Uri _image;
		private BaseAdapter _adapter;
		public ListSection(String caption, Uri image, BaseAdapter adapter)
		{
			_caption = caption;
			_image = image;
			_adapter = adapter;
		}

		public String Caption { get { return _caption; } set { _caption = value; } }
		public Uri Image { get { return _image; } set { _image = value; } }
		public BaseAdapter Adapter { get { return _adapter; } set { _adapter = value; } }
	}
}