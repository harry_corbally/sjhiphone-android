﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using SharedModel.Model;

namespace CapsuleAndroid.CapsuleAdapters
{
	public class SearchAdapter : BaseAdapter<Occurence>
	{
		private Activity _context;  
		private List<Occurence> _items;
		private int _templateResourceId;
		public SearchAdapter(Activity context, 
			int templateResourceId, List<Occurence> items) : base()
		{
			_context = context;
			_templateResourceId = templateResourceId;
			_items = items;
		}
		public override int Count { get { return _items.Count; } }
		public override Occurence this[int index] { get { return _items[index]; } }
		public override long GetItemId(int position) { return position; }
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			Occurence item = this [position];
			View view = convertView;
			if (view == null || !(view is RelativeLayout)) {
				view = _context.LayoutInflater.Inflate (_templateResourceId, parent, false);
			}
			view.Tag = position;
			view.Click += (sender, e) => {
				((RelativeLayout)sender).Enabled = false;

				var occurance = this[Convert.ToInt32(((RelativeLayout)sender).Tag)];
				var subSection = AppDelegate.BookGroups.First(g => g.Id == occurance.GroupId)
					.Items.First(b => b.Id == occurance.BookId)
					.Items.First(c => c.Id == occurance.ChapterId)
					.Items.First(s => s.Id ==  occurance.SectionId)
					.Items.First(ss => ss.Id == occurance.SubSectionId);
				var nextActivity = new Intent(this._context, typeof(HtmlPageActivity));
				nextActivity.PutExtra("ItemTitle", subSection.Title);
				nextActivity.PutExtra("PageUrl", Path.Combine(AppDelegate.RootDataFolder, subSection.absolutepath.Replace(@"\", @"/"), subSection.HtmlPath));
				_context.StartActivity(nextActivity);

				((RelativeLayout)sender).Enabled = true;
			};
			var imgInfo = view.FindViewById<ImageButton> (Resource.Id.imgInfo);
			imgInfo.Visibility = ViewStates.Gone;

			TextView title = view.FindViewById<TextView> (Resource.Id.title);
			title.Text = String.Format("{0} > {1} > {2}", item.ChapterTitle, item.SectionTitle, item.SubSectionTitle);

			TextView subTitle = view.FindViewById<TextView> (Resource.Id.subTitle);
			subTitle.Visibility = ViewStates.Gone;
			var imgItem = view.FindViewById<ImageView> (Resource.Id.imgItem);
			imgItem.Visibility = ViewStates.Gone;

			return view;
		}
	}
}