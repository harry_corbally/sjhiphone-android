﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SharedModel.Model;

namespace CapsuleAndroid
{
	//#if DEBUG

	/*#else
	[Application(Debuggable = false, ManageSpaceActivity = typeof(MainActivity))]
#endif*/
	[Application(Debuggable = true, ManageSpaceActivity = typeof(MainActivity))]
	public class AppDelegate : Android.App.Application
	{
		public static String XmlFileName = "CapsuleBooks.xml";
		public static String CapsuleMainFolderName = "CapsuleMainFolder";
		public static String SyncFolderName = "SyncFolder";
		public static String RootDataFolder { get { return System.IO.Path.Combine (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal), "Snapshot_IsoStore"); } }

		public static IEnumerable<BookGroup> BookGroups;
		public BookItem SelectedBook { get; set; }
		public SectionItem SelectedSection { get; set; }

		public AppDelegate(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{

		}

		public override void OnCreate ()
		{
			base.OnCreate ();
		}
	}
}