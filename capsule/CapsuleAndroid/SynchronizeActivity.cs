﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SharedModel.Services;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using SharedModel.Model;

namespace CapsuleAndroid
{
	[Activity (Label = "Synchronization")]
	[MetaData ("android.support.PARENT_ACTIVITY", Value = "capsuleandroid.MainActivity")]	
	public class SynchronizeActivity : Activity
	{
		//internal const String _rootURL = "http://sjhws-uat.tekenable-test.com:85/api/";
		internal const String _rootURL = "http://livebu-sjh.tekenable-test.com:83/webservices/api/";
		internal ProgressDialog dialog;
		internal DirectoryInfo _localFolder;
		internal DirectoryInfo _capsuleMainFolder;
		internal DirectoryInfo _capsuleMainFolderBackup;
		internal DirectoryInfo _syncFolder;
		private static List<BookGroup> _bookGroups;
		internal TextView txtSyncDate;
		internal DateTime _lastSyncDate;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.Synchronization);
			this.ActionBar.SetDisplayHomeAsUpEnabled (true);

			dialog = new ProgressDialog (this);
			dialog.Indeterminate = true;
			dialog.SetProgressStyle (ProgressDialogStyle.Spinner);
			dialog.SetCancelable (false);

			txtSyncDate = FindViewById<TextView> (Resource.Id.txtSyncDate);
			var prefs = GetSharedPreferences ("Synchronization", FileCreationMode.Private);
			var syncDate = prefs.GetString ("LastSyncDate", null);
			if (syncDate != null) {
				_lastSyncDate = Convert.ToDateTime (syncDate);
				txtSyncDate.Text = String.Format ("{0:dd/MM/yyyy HH:mm}", _lastSyncDate);
			}
		}

		[Java.Interop.Export("On_SyncClicked")]
		async public void On_SyncClicked(View view)
		{
			this.ActionBar.Subtitle = "Initializing...";
			dialog.SetMessage ("Initializing...");
			dialog.Show ();
			Boolean hasException = false;
			try {
			_localFolder = new DirectoryInfo(AppDelegate.RootDataFolder);
			if (!_localFolder.Exists) {
				_localFolder.Create ();
			}

			_capsuleMainFolder = _localFolder.GetDirectories (AppDelegate.CapsuleMainFolderName, SearchOption.TopDirectoryOnly).FirstOrDefault();
			if (_capsuleMainFolder == null || !_capsuleMainFolder.Exists) {
				_capsuleMainFolder = _localFolder.CreateSubdirectory (AppDelegate.CapsuleMainFolderName);
			}

			_capsuleMainFolderBackup = _localFolder.GetDirectories ("CapsuleMainFolderBackup", SearchOption.TopDirectoryOnly).FirstOrDefault();
			if (_capsuleMainFolderBackup != null && _capsuleMainFolderBackup.Exists) {
				await _capsuleMainFolderBackup.DeleteAsync (true);
			}
			_capsuleMainFolderBackup = _localFolder.CreateSubdirectory ("CapsuleMainFolderBackup");

			_syncFolder = _localFolder.GetDirectories (AppDelegate.SyncFolderName, SearchOption.TopDirectoryOnly).FirstOrDefault ();
			if (_syncFolder != null && _syncFolder.Exists) {
				await _syncFolder.DeleteAsync (true);
			}
			_syncFolder = _localFolder.CreateSubdirectory (AppDelegate.SyncFolderName);

			dialog.SetMessage ("Creating Backup...");
			this.ActionBar.Subtitle = "Creating Backup...";

			await CopyFolder (_capsuleMainFolder, _capsuleMainFolderBackup);

			this.ActionBar.Subtitle = "Getting Book XML...";
			dialog.SetMessage ("Getting Book XML...");

			var xmlfile = await SyncServices.GetUpdatedXML (_rootURL);
			using (var ff = new FileStream (Path.Combine (_syncFolder.FullName, AppDelegate.XmlFileName), FileMode.Create, FileAccess.Write)) {
				await xmlfile.CopyToAsync (ff);
			}
			
			if (xmlfile == null)
			{
				//MessageBox.Show("Book XML cannot be null!", "Sync Failed", MessageBoxButton.OK);
				this.SetDialogCancelable ("Sync Failed", "Book XML cannot be null!");
				hasException = true;
			}
			//*

			this.ActionBar.Subtitle = "Getting Sub-Section files...";
			dialog.SetMessage ("Getting Sub-Section files...");
			Byte step = 200;

			var updatedSubSectionFileIDs = await SyncServices.GetSubSectionFileIDsUpdatedSince(_rootURL, _lastSyncDate);
			if (updatedSubSectionFileIDs != null && updatedSubSectionFileIDs.Any())
			{
				for (int i = 0; i < updatedSubSectionFileIDs.Count(); i+=step)
				{
					var ids = updatedSubSectionFileIDs.Select(p => p.ActualID).Skip(i).Take(step).ToArray();
					Stream updatedSubSectionFilesStream = null;

					try
					{
						//updatedSubSectionFilesStream = await SyncServices.GetUpdatedSubSectionFiles(_rootURL,new int[9]); //ids);
							updatedSubSectionFilesStream = await SyncServices.GetUpdatedSubSectionFiles(_rootURL, ids);
					}
					catch (Exception ex)
					{
						//MessageBox.Show(ex.Message, "Sync Failed", MessageBoxButton.OK);
						this.SetDialogCancelable ("Sync Failed", ex.Message);
						hasException = true;
					}

					if (hasException)
					{
						await RestoreIfNeeded(hasException, _capsuleMainFolderBackup, _capsuleMainFolder);
						return;
					}

					if (updatedSubSectionFilesStream == null)
					{
						//MessageBox.Show("Sub-Section zip file cannot be null!", "Sync Failed", MessageBoxButton.OK);
						this.SetDialogCancelable ("Sync Failed", "Sub-Section zip file cannot be null!");
						return;
					}
					//Java.Util.Zip.ZipInputStream zi = new Java.Util.Zip.ZipInputStream (updatedSubSectionFilesStream);
					using (var zip = new Java.Util.Zip.ZipInputStream(updatedSubSectionFilesStream))
					{
						Java.Util.Zip.ZipEntry entry;
						while ((entry = zip.NextEntry) != null)
						{
							//logic to determine where to put the file goes here
							if (entry.IsDirectory) {
								_syncFolder.CreateSubdirectory (entry.Name);
								continue;
							}
							await Task.Run (() => {
								//StorageFile storageFile = await syncFolder.CreateFileAsync(entry.FullName, CreationCollisionOption.ReplaceExisting);
								FileInfo storageFile = new FileInfo (Path.Combine (_syncFolder.FullName, entry.Name.Replace("\\", "/")));
								// create the directory if it doesn't exist
								storageFile.Directory.Create ();
								//using (Stream outputStream = await storageFile.OpenStreamForWriteAsync())
								using (var outputStream = storageFile.Open (FileMode.Create)) {
									//await entry.Open().CopyToAsync(outputStream);

									byte[] buffer = new byte[1024];
									int count;
									while ((count = zip.Read (buffer)) != -1) {
										outputStream.Write (buffer, 0, count);
									}
								}
							});
						}
					}
				}
			}

			this.ActionBar.Subtitle = "Getting Index files...";
			dialog.SetMessage ("Getting Index files...");

			var updatedIndexIds = await SyncServices.GetIndexFileIDsUpdatedSince(_rootURL, _lastSyncDate);
			if (updatedIndexIds != null && updatedIndexIds.Any())
			{
				for (int i = 0; i < updatedIndexIds.Count(); i+=step)
				{
					Stream updatedIndexStream = null;

					try
					{
						updatedIndexStream = await SyncServices.GetUpdatedIndexFiles(_rootURL, updatedIndexIds.Skip(i).Take(step).ToArray());
					}
					catch (Exception ex)
					{
						//MessageBox.Show(ex.Message, "Sync Failed", MessageBoxButton.OK);
						this.SetDialogCancelable ("Sync Failed", ex.Message);
						hasException = true;
					}

					if (hasException)
					{
						await RestoreIfNeeded(hasException, _capsuleMainFolderBackup, _capsuleMainFolder);
						return;
					}

					if (updatedIndexStream == null)
					{
						//MessageBox.Show("Index zip file cannot be null!", "Sync Failed", MessageBoxButton.OK);
						this.SetDialogCancelable ("Sync Failed", "Index zip file cannot be null!");
						return;
					}

					using (var zip = new Java.Util.Zip.ZipInputStream(updatedIndexStream))
					{
						Java.Util.Zip.ZipEntry entry;
						while ((entry = zip.NextEntry) != null)
						{
							//logic to determine where to put the file goes here
							await Task.Run (() => {
								FileInfo storageFile = new FileInfo (Path.Combine (_syncFolder.FullName, entry.Name));
								using (Stream outputStream = storageFile.OpenWrite ()) {
									byte[] buffer = new byte[1024];
									int count;
									while ((count = zip.Read (buffer)) != -1) {
										outputStream.Write (buffer, 0, count);
									}
								}
							});
						}
					}
				}
			}

			this.ActionBar.Subtitle = "Managing files...";
			dialog.SetMessage ("Managing files...");

			DirectoryInfo contentFolder = _capsuleMainFolder.CreateSubdirectory("Content");
				DirectoryInfo indexFilesFolder = _capsuleMainFolder.CreateSubdirectory("IndexFiles");

			// copy javascript and css files from installation folder into content folder
			using (var capsuleScript = Assets.Open ("CapsuleScript.js")) {
				await capsuleScript.CopyToAsync (new FileStream(Path.Combine(contentFolder.FullName, "CapsuleScript.js"), FileMode.Create, FileAccess.Write));
			}

			using (var capsuleStyle = Assets.Open ("CapsuleStyle.css")) {
				await capsuleStyle.CopyToAsync (new FileStream(Path.Combine (contentFolder.FullName, "CapsuleStyle.css"), FileMode.Create, FileAccess.Write));
			}

			using (var jquery = Assets.Open ("jquery-1.9.1.js")) {
				await jquery.CopyToAsync (new FileStream(Path.Combine (contentFolder.FullName, "jquery-1.9.1.js"), FileMode.Create, FileAccess.Write));
			}

			using (var searchHighlight = Assets.Open ("SearchHighlight.js")) {
				await searchHighlight.CopyToAsync (new FileStream(Path.Combine (contentFolder.FullName, "SearchHighlight.js"), FileMode.Create, FileAccess.Write));
			}
			xmlfile.Position = 0;
			// copy CapsuleBooks.xml file into the proper folder
			using (var ff = new FileStream (Path.Combine (_capsuleMainFolder.FullName, "CapsuleBooks.xml"), FileMode.Create, FileAccess.Write)) {
				await xmlfile.CopyToAsync (ff);
			}

			// copy the .index files into the proper folder
			var files = _syncFolder.GetFiles ();
			var indexFiles = files.Where(p => p.Name.EndsWith(".index"));

			foreach (var indexFile in indexFiles)
			{
				await indexFile.OpenRead().CopyToAsync(new FileStream (Path.Combine(indexFilesFolder.FullName, indexFile.Name), FileMode.Create, FileAccess.Write));
			}

			// parse xml
			using (StreamReader streamReader = new StreamReader (Path.Combine(_capsuleMainFolder.FullName, "CapsuleBooks.xml")))
			{
				var xmlContent = await streamReader.ReadToEndAsync();
				_bookGroups = new List<BookGroup>();
				_bookGroups.AddRange(CommonServices.ParseXml(xmlContent, contentFolder.FullName));
			}

			// creating the full folder structure against the xml

			// delete obsolete group folders
			var allGroupFolders = contentFolder.GetDirectories ();
			if (allGroupFolders != null)
			{
				IEnumerable<String> groupFolderNames = allGroupFolders.Select(p => p.Name);
				IEnumerable<String> groupNames = _bookGroups.Select(p => p.Id.ToString());

				await DeleteObsoleteFolders(contentFolder, groupFolderNames, groupNames);
			}

			String syncGroupFolderName = String.Empty;
			String syncBookFolderName = String.Empty;
			String htmlFileName = String.Empty;

			try
			{
				if (updatedSubSectionFileIDs != null && updatedSubSectionFileIDs.Any())
				{
					foreach (var bookGroup in _bookGroups)
					{
						var groupFolder = contentFolder.CreateSubdirectory(bookGroup.Id.ToString());

						syncGroupFolderName = bookGroup.Id.ToString();
						var syncGroupFolder = _syncFolder.GetDirectories(syncGroupFolderName, SearchOption.TopDirectoryOnly).FirstOrDefault();

						foreach (var book in bookGroup.Items)
						{
							var bookFolder = groupFolder.CreateSubdirectory(book.Id.ToString());

							syncBookFolderName = book.Id.ToString();
							var syncBookFolder = syncGroupFolder.GetDirectories(syncBookFolderName, SearchOption.TopDirectoryOnly).First();

							string coverImgName = Path.GetFileName(book.ImageString);
							var coverImg = syncBookFolder.GetFiles(coverImgName, SearchOption.TopDirectoryOnly).First();

							await coverImg.OpenRead().CopyToAsync(new FileStream(Path.Combine(bookFolder.FullName, coverImgName), FileMode.Create, FileAccess.Write));

							foreach (var chapter in book.Items)
							{
								var chapterFolder = bookFolder.CreateSubdirectory(chapter.Id.ToString());

								string chapterImgName = Path.GetFileName(chapter.ImageString);
								var chapterImg = syncBookFolder.GetFiles(chapterImgName, SearchOption.TopDirectoryOnly).First();

								await chapterImg.OpenRead().CopyToAsync(new FileStream(Path.Combine(chapterFolder.FullName, chapterImgName), FileMode.Create, FileAccess.Write));

								foreach (var section in chapter.Items)
								{
									var sectionFolder = chapterFolder.CreateSubdirectory(section.Id.ToString());

									// delete obsolete sub-section folders
									var allSubSectionFolders = sectionFolder.GetDirectories();
									if (allSubSectionFolders != null)
									{
										IEnumerable<String> subSectionFolderNames = allSubSectionFolders.Select(p => p.Name);
										IEnumerable<String> subSectionNames = section.Items.Select(p => p.Id.ToString());

										await DeleteObsoleteFolders(sectionFolder, subSectionFolderNames, subSectionNames);
									}

									await chapterImg.OpenRead().CopyToAsync(new FileStream(Path.Combine(sectionFolder.FullName, chapterImgName), FileMode.Create, FileAccess.Write));

									foreach (var subsection in section.Items.Where(p => updatedSubSectionFileIDs.Select(q => q.OriginalID).Contains(p.Id)))
									{
										var subsectionFolder = sectionFolder.CreateSubdirectory(subsection.Id.ToString());

										var allFiles = subsectionFolder.GetFiles();

										if (allFiles != null)
										{
											var allHTML = allFiles.Where(p => Path.GetExtension(p.Name).ToLower() == "html");

											if (allHTML != null)
											{
												var htmlsToDelete = allHTML.Where(p => p.Name != subsection.HtmlPath);

												foreach (var item in htmlsToDelete)
												{
													item.Delete();
												}
											}
										}

										await chapterImg.OpenRead().CopyToAsync(new FileStream(Path.Combine(subsectionFolder.FullName, chapterImgName), FileMode.Create, FileAccess.Write));

										var htmlFile = syncBookFolder.GetFiles(subsection.HtmlPath).First();

										await htmlFile.OpenRead().CopyToAsync(new FileStream(Path.Combine(subsectionFolder.FullName, htmlFile.Name), FileMode.Create, FileAccess.Write));
									}
								}
							}
						}
					}
				}
				txtSyncDate.Text = String.Format ("{0:dd/MM/yyyy HH:mm}", DateTime.Now);
				var prefEditor = GetSharedPreferences ("Synchronization", FileCreationMode.Private).Edit();
				prefEditor.PutString ("LastSyncDate", String.Format ("{0:dd/MMM/yyyy HH:mm}", DateTime.Now));
				prefEditor.Commit();

				SetResult(Result.Ok);
			}
			catch (Exception ex)
			{
				this.SetDialogCancelable("Sync Failed", String.Format("Exception occured on sync process at step 'Copy HTML files'. Path '{0}'", Path.Combine(_syncFolder.Name, syncGroupFolderName, syncBookFolderName)));
				hasException = true;
			}
			} catch(Exception ex) {
				this.SetDialogCancelable("Sync Failed", String.Format("Exception occured on sync process, {0}", ex.Message));
				hasException = true;
			}
			await RestoreIfNeeded(hasException, _capsuleMainFolderBackup, _capsuleMainFolder);
			if (!hasException) {
				this.ActionBar.Subtitle = null;
				dialog.Dismiss ();
			}
		}

		/*public void DialogCancel(object sender, TEventArgs e) {
			dialog.Dismiss ();
		}*/

		private async Task<Boolean> RestoreIfNeeded(Boolean hasException, DirectoryInfo capsuleMainFolderBackup, DirectoryInfo capsuleMainFolder)
		{
			if (hasException && capsuleMainFolderBackup != null && capsuleMainFolderBackup.Exists)
			{
				//dialog.SetMessage ("Restoring Content...");
				//this.ActionBar.Subtitle = "Restoring Content...";

				capsuleMainFolder = _localFolder.CreateSubdirectory("CapsuleMainFolder");

				await CopyFolder(capsuleMainFolderBackup, capsuleMainFolder);

				return false;
			}

			if (!hasException && capsuleMainFolderBackup != null && capsuleMainFolderBackup.Exists)
			{
				await capsuleMainFolderBackup.DeleteAsync (true);
			}

			return true;
		}

		private async Task CopyFolder(DirectoryInfo source, DirectoryInfo destination)
		{
			var files = source.GetFiles();

			foreach (var file in files)
			{
				await Task.Run(() => { File.Copy (file.FullName, Path.Combine(destination.FullName, file.Name), true); });
			}

			var folders = source.GetDirectories();

			foreach (var folder in folders)
			{
				var subFolder = destination.GetDirectories (folder.Name, SearchOption.TopDirectoryOnly).FirstOrDefault();
				if (subFolder != null && subFolder.Exists) {
					subFolder.Delete (true);
				}
				DirectoryInfo folderBU = destination.CreateSubdirectory(folder.Name);

				await CopyFolder(folder, folderBU);
			}
		}

		private async Task DeleteObsoleteFolders(DirectoryInfo deleteFrom, IEnumerable<string> allFolderNames, IEnumerable<string> currentFolderNames)
		{
			await Task.Run (() => {
				IEnumerable<String> deletedFolders = allFolderNames.Except (currentFolderNames);

				if (deletedFolders != null) {
					foreach (var deletedFolder in deletedFolders) {
						var folderToDelete = deleteFrom.GetDirectories (deletedFolder).First ();
						folderToDelete.Delete (true);
					}
				}
			});
		}

		private void SetDialogCancelable(String title, String message) {
			dialog.SetTitle (title);
			dialog.SetMessage (message);
			dialog.SetCancelable (true);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Android.Resource.Id.Home:
				//SetResult(Result.Ok);
				//Android.Support.V4.App.NavUtils.NavigateUpFromSameTask (this);
				Finish ();
				return true;
			}
			return base.OnOptionsItemSelected (item);
		}
	}

	public static class ExtMethods
	{
		async public static Task DeleteAsync(this DirectoryInfo dir, Boolean recursive)
		{
			await Task.Run (() => {
				dir.Delete (recursive);
			});
		}
	}
}

